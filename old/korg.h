		/* MIDI Standard Code Specifications */

#define KEYOFF    0x80
#define KEYON     0x90
#define POLYPRESS 0xA0
#define CTRLCHNG  0xB0
#define PROGCHNG  0xC0
#define CHANPRESS 0xD0
#define PITCHBND  0xE0
#define SYSMSG    0xF0

#define SYSEX     0xF0
#define EOX       0xF7

		/* Korg M1 System Exclusive stuff */

/* Header information */

#define KORGID    0x42
#define FMTID	  0x30
#define M1ID      0x19

/* Function Codes */

#define REQMODE   0x12
#define REQ1PGM   0x10
#define REQPGMS   0x1C
#define REQ1CMB   0x19
#define REQCMBS   0x1D
#define REQSEQS   0x18
#define REQGLBL   0x0E
#define REQALL    0x0F

#define DMPMODE   0x42
#define DMP1PGM   0x40
#define DMPPGMS   0x4C
#define DMP1CMB   0x49
#define DMPCMBS   0x4D
#define DMPSEQS   0x48
#define DMPGLBL   0x51
#define DMPALL    0x50

#define WRT1PGM   0x11
#define WRT1CMB   0x1A

#define CHGMODE   0x4E
#define CHGPARM   0x41

#define FMTERR    0x26
#define LOADOK    0x23
#define LOADERR   0x24
#define WRTOK     0x21
#define WRTERR    0x22

		/* Control Destinations */

#define VEL_CTRL  0x07

		/* Data Masks */

#define STATEMASK         0x80
#define CHANNELMASK       0x0F
#define STATE_SELECT_MASK 0xF0

		/* Fundamental Routines */

#define midi_in(c)    c = Bconin(3) & 0x00FF
#define midi_out(c)   Bconout(3,c & 0x00FF)
#define midi_print(c) printf("%02x ",c & 0x00FF)


struct ndxstr  { UBYTE instno,key,pan,tune,level,decay,null; };
struct drumstr { struct ndxstr drum_ndx[30]; };


struct glbstr 
{
 UBYTE	mstr_tune,key_trans,dmpr_polrty,as_ped_1,as_ped_2,scale_type,
	pure_type_key,user_scale[12];
 UBYTE  endglb[2];
 struct drumstr drum[4];
};


struct all
{
 struct glbstr global;
 struct cmbstr combi;
 struct pgmstr progam;
 struct seqstr seqnc;
};
