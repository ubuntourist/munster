		Instructions for dumping and loading the Korg M1
			       By Kevin Cole
				28-Feb-92

This setup is far from ideal, but it works... most of the time.  And I'm
working on some improvements.  So, these instructions are likely to become
shorter as the semester progresses...  I hope. (In fact, I hope to eliminate 
several steps by next week, but I've had some problems with that.)

1.  Connect the MIDI cables between the side of the Atari computer and the M1.
    The brown cable is currently Atari MIDI IN to Korg MIDI OUT and the black 
    is Atari MIDI OUT to Korg MIDI IN.  That's subject to change without 
    notice.  In case it does change, the outlets are on the left side of the 
    Atari keyboard.  The one closest to the front is MIDI IN and the other is 
    (logically enough) MIDI OUT.

2.  Power up the Atari.  The screen should go white, a message about hard disks
    should appear, and finally, 5 black disks on a blue background should 
    appear and should only go about halfway across the screen.  If anything 
    else happens, (like very large pictures of the disks, or white disks on a 
    green background or nothing happens after about a minute, then the Atari
    didn't power up correctly.  Power it down, then wait 30 seconds and try
    again. 

3.  Move the mouse to RDY.PGM, and hit the left mouse button twice in rapid 
    succession.  (This is called double-clicking the mouse.)  You should see a 
    little "busy bee" appear.  If not, try it again, since that mouse button's
    not in the best condition.  The screen should clear and a new menu bar
    should appear at the top of the screen.  (If you accidentally get a pull-
    down menu, just move somewhere into the blue and click the mouse.)

4.  Move to Options.  Select Load a RAM Disk from the pull-down menu.  Use the 
    backspace key to remove what is currently displayed (A:RAMDRIVE, or 
    whatever) and replace it with "HDRD500"  (minus the quotes).  Click on OK.
    The machine should flash once and reboot.  Now the RAM disk is "active".

5.  Move the mouse to the 4th disk, Disk D:, and double-click.  A directory 
    should appear.  Double-click on BIN.  A sub-directory should appear.  Move 
    the mouse to the lower right corner, and click on the down arrow until you 
    see MSH.PGM appear on the screen.  (You can also get there by clicking the 
    mouse on the unshaded box between the two arrows, holding the mouse button
    down and "dragging" the box about halfway down the screen.  Double-click on 
    MSH.PGM and wait for the busy bee.  The screen should clear, and you should 
    see "D>".

6.  To save sequences, programs and combinations from the M1 to the Atari, 
    at the D> prompt, type:

		cd d:\korg
		dumper

    Make sure that the System Exclusive (EXCL) in the Global page is enabled
    (ENA). Follow the directions that appear and wait about 2 minutes.  If it 
    doesn't come back saying that the dump is in memory, something's gone wrong.
    I have not determined what causes this type of failure.  If this happens,
    hit the reset button (a small spring-loaded square button on the back left
    of the keyboard).  The system will reboot, and you have to start over at
    step 5.  Sorry 'bout that.  (You can TRY control-C before resorting to the
    reset.  But no guarantees there.  If it works, you can just retype "dumper"
    at the D> prompt.) 
Instructions for dumping and loading the Korg M1			Page 2


7.  I would encourage you to use the naming scheme I've been using for
    filenames, but it's optional.  The format of my filenames is:

	yymmdd#s.typ

    where yy = year,  mm = month, dd = day, # = number of the save today
    starting with zero,  s = station (1, 2 or 3), and typ = the type of dump 
    (ALL, PGM, CMB, or SEQ).  Currently ALL is the only available type.
    So, for example,  92022532.ALL would contain the data from the third time
    that station 2 was dumped on 02/28/92. 

    Do *NOT* use filenames ending in .C, .O or .PRG, as these are reserved by 
    the Atari.  There are some others, but I don't recall what they are off the 
    top of my head.  Dumper now checks for previously existing files, and asks 
    you if you wish to overwrite them.  If you get this question, it only looks 
    at the first letter of your answer.  "Y" or "y" is assumed to mean "Yes,
    overwrite the existing file".  All other answers are assumed to mean "No".

    Dumper should usually report 45000 and 56000 bytes saved.  If you see a 
    number outside of that range, then something has probably gone amiss.  This 
    can happen if you play the keyboard while it's dumping, or for other causes
    unknown.  Try again.  (Just type "dumper" if it gives you the "D>" prompt.)

8.  To load a file from the Atari into the M1, follow the same procedure as 
    above except type "loader" instead of "dumper".  Be sure that your memory 
    configuration on the M1 is the same as that of the file being loaded and 
    that the memory protection is off for all three memories (Programs, 
    Combinations and Sequences.)  Then just give it the appropriate filename.
    Loader will come back with a status message telling you whether or not the 
    load was successful.  Common causes for failure are:  loading a file with
    100 programs into a memory configured for 50 programs, or vice versa, 
    loading to protected memory, or loading a file so big that 0% memory is 
    free.  I *THINK* this last one is actually okay, even though the M1 reports 
    that there was an error in loading.

9.  To see what files are available, type "ls -l *.all" sans quotes, at the D>
    prompt.
