# Captain's Log

## The Trouble with Munsters

These are the chronicles of an attempt to resurrect portions of the
Korg M1 backup and restore system known as `munster` (a play on
"M1-ster"). The system was originally developed for the Atari 1040-ST
circa 1992.

**NOTE:** At some point I managed to at least compile `munster`
successfully under Linux. There is a working executable `munster` in
`~/BestOfCD/PCHB1F/Code/C/MIDI/`. (Well, "working" might be an
exaggeration: It displays the menu and, `exit` works okay. God knows
what it attempts to do with anything else, since there's no Korg M1
hooked up...)

### 2019.10.26 (KJC)

* Clever monkey. the `${variable:offset:length}` trick lets us get the
  _root_ of the filenames sans the extension. Since we're interested
  in compiling the C code, the extension is two characters long
  (`.c`). So, this will attempt to compile all of them:  
  
    `for i in *.c; do  make ${i:0:-2}; done`
  

* Out of 39 files, only three, `selfdoc`, `uudec` and `uudecode`
  compiled right out of the box.

* Apparently, it is now _de rigueur_ to include the standard library,
  whereas in days of yore it was included automatically? Or is there
  some other cleverness that makes it automatically included?  For the
  moment, the addition of:  
  
    `#include <stdlib.h>`  
  
    appears to solve that wrinkle.

* C has gotten stricter since I last used it. A `return` statement
  must include a value, or the fuction must be declared to be of type
  `void` which actually makes sense. Functions that I've declared to
  be of type `int` don't appear to return anything. I don't recall if
  the behavior was to assume a `0` return value or that whatever is
  returned is ignored.

----

### 2021.04.03 (KJC)


Attemting to complile the smallest C files in `/src` first...  This
led to an error in `Cnecin`. A web search turned up **GEMDOS** 
documentation. Specifically:

* [5.16 Character input/output](https://freemint.github.io/tos.hyp/en/gemdos_chrinout.html#Cnecin)

I gather from that, it's "C-n-ec-in" which is to say "Character no
echo input" (**C**haracter **`n**o **ec**ho **in**put).

`GEM` rang a bell, and so a search for "gem" "linux" and "gcc" turned up
the [Hatari](https://hatari.tuxfamily.org/doc/manual.html) emulator!

It may be easier to get munster up and running by emulating an Atari...
And, lo and behold, it's a mere `apt` away...

```
    $ apt install hatari
    The following NEW packages will be installed:
      hatari
    0 upgraded, 1 newly installed, 0 to remove and 0 not upgraded.
    Need to get 3,703 kB of archives.
    After this operation, 16.1 MB of additional disk space will be used.
```

Sadly, it's a bit more complicated than that. `hatari` emulates the
**hardware** but you still have to get **software** namely an operating 
system. `TOS` is rather hard to find (or so I gather) and so one must
use `EmuTOS` which **isn't** a Debian package. How droll. After finding,
downloading, unpacking the zip, the following appears to work:

```
    $ cd ~/Packages/misc/emutos-512k-1.0.1
    $ hatari -t etos512k.img
```

----

### 2021.04.05 (KJC)

Found the old Korg M1 Owner's Manual and printed out the MIDI stuff at
the back.

* `.pgm` files are in **(14) ALL PROGRAM PARAMETER DUMP**.

> |     Bytes     | Description                            |   Notes  |  Hex  |
> |:-------------:|----------------------------------------|:--------:|:-----:|
> | `F0 42 3n 19` | EXCLUSIVE HEADER                       |          |       |
> |  `0100 1101`  | ALL PROGRAM PARAMETER DUMP             |          |  `4C` |
> |  `0000 00mc`  | Mem. Allocation. Bank                  | 3-1, 3-2 |       |
> |  `0ddd dddd`  | Data                                   |     7    |       |
> |     `...`     |                                        |          |       |
> |  `1111 0111`  | EOX                                    |          |  `FE` |

* `.cmb` files are in **(16) ALL COMBINATION PARAMETER DUMP**.

> |     Bytes     | Description                            |   Notes  |  Hex  |
> |:-------------:|----------------------------------------|:--------:|:-----:|
> | `F0 42 3n 19` | EXCLUSIVE HEADER                       |          |       |
> |  `0100 1101`  | ALL COMBINATION PARAMETER DUMP         |          |  `4D` |
> |  `0000 00mc`  | Mem. Allocation. Bank                  | 3-1, 3-2 |       |
> |  `0ddd dddd`  | Data                                   |     9    |       |
> |     `...`     |                                        |          |       |
> |  `1111 0111`  | EOX                                    |          |  `FE` |

* `.all` files are in **(19) ALL DATA (GLOBAL, COMBI, PROG, SEQ) DUMP**

> |     Bytes     | Description                            |   Notes  |  Hex  |
> |:-------------:|----------------------------------------|:--------:|:-----:|
> | `F0 42 3n 19` | EXCLUSIVE HEADER                       |          |       |
> |  `0101 0000`  | ALL DATA (GLBL, COMBI, PROG, SEQ) DUMP |          |  `50` |
> |  `0000 00mc`  | Mem. Allocation. Bank                  | 3-1, 3-2 |       |
> |  `0sss ssss`  | Seq. Data Size                         |   10-1   |       |
> |     `...`     |                                        |          |       |
> |  `0ddd dddd`  | Data                                   |    12    |       |
> |     `...`     |                                        |          |       |
> |  `1111 0111`  | EOX                                    |          |  `FE` |

* `.sng` files are ... a mystery. They don't start with the
  **EXCLUSIVE HEADER**. Instead, they begin `00 10 00 3C` followed by a
  list of 16-character instrument names, followed by a bunch of `00` and
  eventually the song title... and other stuff.

----

For checking, byte-by-byte:

```
$ hexdump -C Data/92021101.all  | most
```

To skip the first 8 bytes, and show the next 4:

```
$ hexdump -s 8 -n 4 -C Data/92021101.all  | most
```

----

## Unexpectedly finding alsa-info

```
$ alsa-info 
ALSA Information Script v 0.4.64
--------------------------------

This script visits the following commands/files to collect diagnostic
information about your ALSA installation and sound related hardware.

  dmesg
  lspci
  aplay
  amixer
  alsactl
  /proc/asound/
  /sys/class/sound/
  ~/.asoundrc (etc.)

See '/usr/sbin/alsa-info --help' for command line options.

Newer version detected: 0.5.0
To view the ChangeLog, please visit
http://www.alsa-project.org/alsa-info.sh.changelog
ALSA-Info script has been downloaded /tmp/alsa-info.Utc5hykeIe.
Please, re-run it from new location.

$ . /tmp/alsa-info.Utc5hykeIe 
ALSA Information Script v 0.5.0
--------------------------------

This script visits the following commands/files to collect diagnostic
information about your ALSA installation and sound related hardware.

  dmesg
  lspci
  aplay
  amixer
  alsactl
  rpm, dpkg
  /proc/asound/
  /sys/class/sound/
  ~/.asoundrc (etc.)

See 'bash --help' for command line options.

Automatically upload ALSA information to www.alsa-project.org? [y/N] : y
Uploading information to www.alsa-project.org ... Done!

Your ALSA information is located at
http://alsa-project.org/db/?f=0f78a1ac531a51944446de98ecf86d6f5911f35e
Please inform the person helping you.

```

The link above produced the 3249-line report, which was then slightly
hand-edited and saved as [alsa-info.txt](./alsa-info.txt).

See also:

* "[The Linux MIDI-HOWTO](https://tldp.org/HOWTO/MIDI-HOWTO.html)"
  from 2002... Specifically [Chapter 9: MIDI
  Development](https://tldp.org/HOWTO/MIDI-HOWTO-9.html).

----

