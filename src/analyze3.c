#include <stdio.h>
#include <stdlib.h>
#include "osbind.h"
#include "misc.h"
#include "korg.h"

unsigned char buf[80],fid[80],minibuf[7];
unsigned char rspns[7] = {0xF0,0x42,0x30,0x19,0x50,0x00,0xF7};
unsigned int  x;

/************************************************************************
 *                                                                      *
 *      expand                                                          *
 *                                                                      *
 *  Takes in the internal Korg array in 8-bit bytes, a.k.a. "true" size *
 * and returns the external (i.e. MIDI or fake) size of the array as    *
 * 7-bit bytes.                                                         *
 *                                                                      *
 ************************************************************************/

int expand(truesize)
int truesize;
{
 int fakesize;

 fakesize = (truesize / 7) * 8;                 /* Seven 8-bit to eight 7-bit */
 if ((truesize % 7) > 0)
   fakesize += (1 + (truesize % 7));            /* Take care of leftovers */
 return(fakesize);
}

/************************************************************************
 *                                                                      *
 *      compress                Written by Kevin Cole   18-Apr-92       *
 *                                                                      *
 *  This is GREAT!  It successfully decyphers portions of the rather    *
 * cryptic format of Korg M1 dumps.  Data from the Korg is 8-bits,      *
 * internally.  However, to send this as MIDI, the high order bit must  *
 * be stripped for non-command data.  This presents a problem.  Korg    *
 * chooses to get around this problem by turning seven 8-bit bytes into *
 * eight 7-bit bytes.  The first byte in the sequence contains the high *
 * order bit for each of the remaining seven bytes.                     *
 *                                                                      *
 ************************************************************************/

unsigned char *compress(mem,itemsize)
unsigned char **mem;
unsigned int  itemsize;
{
 extern unsigned char *calloc();
 unsigned char *item;
 unsigned int n,cmbpgm,pos;

 if ((item = calloc(itemsize,1)) == NULL)       /* Get LOTS of memory     */
 {
   printf("The memory allocation failed... Aborting.\n");
   exit(EXIT_FAILURE);                          /* Return a failure code  */
 }

 for (pos=0; pos<itemsize; ++pos)
 {
  if (x == 7)                                   /* minibuf is empty       */
  {
   for (x=0; x<7; ++x)                          /* Fit 8 bytes into 7     */
    minibuf[x] = (**mem & (1<<x)) ? (*(*mem+1+x) | 0x80) : *(*mem+1+x);
   *mem+=8;                                     /* Skip 8 bytes           */
   x = 0;                                       /* Goto start of minibuf  */
  }
  item[pos] = minibuf[x++];                     /* Store assembled byte   */
 }
 return item;                                   /* Tell 'em where it's at */
}

/************************************************************************
 *                                                                      *
 *       analyze.c              Written by Kevin Cole   30-Jan-92       *
 *                              Last revised:           13-Apr-92       *
 *                                                                      *
 *  This program is based on loader.c.  It's supposed to load a MIDI    *
 * dump back to a Korg M1...                                            *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.02.15.01  KJC  Added GEM support for user-friendliness.      *
 *      92.02.15.01  KJC  Removed GEM support cuz it don't work.        *
 *      92.02.25.01  KJC  Supports filename on command line.            *
 *      92.02.25.02  KJC  Checks for non-existent file.                 *
 *      92.02.25.03  KJC  Checks Korg return status for success/failure *
 *      92.03.02.01  KJC  Changed fid from a pointer to an array, due   *
 *                        to technical difficulties in another program. *
 *      92.03.03.01  KJC  Report on different types of dumps.           *
 *      92.03.04.01  KJC  Changed some of the while loops to improve    *
 *                        speed... I hope.                              *
 *      92.03.24.01  KJC  Added code to change the mode to GLOBAL.      *
 *      92.03.31.01  KJC  Now the program chooses its own directory...  *
 *      92.04.10.01  KJC  Retries on Global Memory Allocation failures  *
 *                        instead of exiting.                           *
 *      92.04.21.01  KJC  Added a computed file length check.           *
 *                                                                      *
 ************************************************************************/

void main(argc,argv)
int argc;  char *argv[];
{
 extern unsigned char *lcalloc();
 unsigned char *mem,*head,*dstart;              /* Pointers to characters */
 unsigned char *item;
 unsigned int strm,n,byts,success,maxpgm;
 unsigned int seqlen,cmbpgm,cluster,pos,guess;
 long dumpsize;
 FILE *fin;

 item = NULL;

 if (argc == 2)                                 /* Name on command line   */
 {
  strcpy(fid,argv[1]);
  printf("Loading Korg from %s.\n",fid);
  fflush(stdout);
 }
 else
 {
  strcpy(fid,"d:m1dump.all");                   /* Use default name.      */
  printf("Enter name of source file [%s]: ",fid);
  fflush(stdout);                               /* Ask it NOW!            */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%s",fid);                        /* Or, use a real name    */
 }

 while ((fin = fopen(fid,"rb")) == NULL)        /* Non-existent file      */
 {
  printf("File %s does not exist.  Quit? [N]: ",fid);
  fflush(stdout);                               /* Ask it NOW!            */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   if ((buf[0] & 0x5F) == 'Y')                  /* What's the answer?     */
     exit(EXIT_FAILURE);
   else
   {
     printf("Enter name of source file [%s]: ",fid);
     fflush(stdout);                            /* Ask it NOW!            */
     if (fgets(buf,sizeof(buf),stdin) != NULL)
      sscanf(buf,"%s",fid);                     /* Or, use a real name    */
   }
 }

 (void) fseek(fin,0L,2);                        /* Seek EOF               */
 dumpsize = ftell(fin);                         /* How big is it?         */
 head = NULL;                                   /* "Break" it first       */
 if ((head = lcalloc(dumpsize,1L)) == NULL)     /* Get LOTS of memory     */
 {
   printf("The memory allocation failed... Aborting.\n");
   exit(EXIT_FAILURE);                          /* Return a failure code  */
 }
 (void) fseek(fin,0L,0);                        /* Rewind file            */
 (void) fread(head,(int) dumpsize,1,fin);       /* Fill memory with it    */
 fclose(fin);                                   /* We're done w/ the file */
 printf("%ld bytes read from %s\n",dumpsize,fid);

 for (mem=head; mem<=head+dumpsize; ++mem)      /* Search all used memory */
 {
  success = 0;                                  /* Assume no match found  */
  for (x=0; x<4; ++x)                           /* Compare 6-byte window  */
  {
   if ((*(mem+x) & rspns[x]) != rspns[x])       /* Contents of (mem+x)    */
     success = 0;                               /* Break stride.          */
   else
     ++success;                                 /* Another successive hit */
  }
  if (success == 4) break;                      /* Entirely successful?   */
 }

 if (success != 4)                              /* Couldn't find 6 bytes  */
 {
  printf("Something failed... No Dump header found.\n");
  free(head);                                   /* Give head... back.     */
  exit(EXIT_FAILURE);
 }

 dstart = mem;                                  /* True start of dump     */

 switch (*(dstart+4))
 {
  case 0x48:
    printf("Loading All Sequences\n");
    seqlen = *(dstart+6) + (*(dstart+7)<<7);    /* Sequences length       */
    guess  = 9+expand(1522+(seqlen*4));         /* Guess file size        */
    mem    = dstart + 8;
    break;
  case 0x4C:
    printf("Loading All Programs\n");
    maxpgm = (*(dstart+5) & 2) ? 50 : 100;      /* Highest Comb or Prog # */
    guess  = 7+expand(maxpgm*143);              /* Guess file size        */
    mem    = dstart + 6;
    break;
  case 0x4D:
    printf("Loading All Combinations\n");
    maxpgm = (*(dstart+5) & 2) ? 50 : 100;      /* Highest Comb or Prog # */
    guess  = 7+expand(maxpgm*124);              /* Guess file size        */
    mem    = dstart + 6;
    break;
  case 0x51:
    printf("Loading All Global Data\n");
    guess  = 7+expand(861);                     /* Guess file size        */
    mem    = dstart + 8;
    break;
  case 0x50:
    printf("Loading Everything (Global, Prog, Comb, & Seq)\n");
    maxpgm = (*(dstart+5) & 2) ? 50 : 100;      /* Highest Comb or Prog # */
    seqlen = *(dstart+6) + (*(dstart+7)<<7);    /* Sequences length       */
    guess  = 9+expand(861);                     /* Guess file size (glob) */
    guess += expand(maxpgm*124);                /* Guess file size (comb) */
    guess += expand(maxpgm*143);                /* Guess file size (prog) */
    guess += expand(1522+(seqlen*4));           /* Guess file size (seq)  */
    mem    = dstart + 8;
    break;
  default:
    printf("This is not a Korg M1 dump.  Continue? [N]: ");
    printf("%02X\n",*(dstart+4));               /***DEBUG***/
    fflush(stdout);                             /* Ask it NOW!            */
    if (fgets(buf,sizeof(buf),stdin) != NULL)
    if ((buf[0] & 0x5F) != 'Y')                 /* What's the answer?     */
    {
      free(head);                               /* Give head... back.     */
      exit(EXIT_FAILURE);
    }
    break;
 }

 maxpgm = (*(dstart+5) & 2) ? 50 : 100;         /* Highest Comb or Prog # */
 printf("%u Programs/%u Combinations\n",maxpgm,maxpgm);

 if ((*(dstart+4) == 0x50) || (*(dstart+4) == 0x48))
 {
   seqlen = *(dstart+6) + (*(dstart+7)<<7);     /* Let's compute a length */
   printf("Sequencer data: %u bytes long.\n",seqlen);
 }

 printf("Guestimated file size: %u bytes.\n",guess);

 Cnecin();                                      /* Wait a sec...          */
 x = 7;                                         /* Preslug minibuf index  */

 if ((*(dstart+4) == 0x50) || (*(dstart+4) == 0x4D))
 {
   printf("\n            Combinations\n\n");
   for (cmbpgm=0; cmbpgm<maxpgm; ++cmbpgm)      /* For 50 or 100 combs    */
   {
    item = compress(&mem,124);
    printf("I%02d: ",cmbpgm);                   /* Combination number     */
    for (n=0; n<10; ++n)                        /* Combination name       */
     fputc(item[n],stdout);
    printf("\n");
    free(item);
   }
   Cnecin();                                    /* Wait for keypress      */
 }

 if ((*(dstart+4) == 0x50) || (*(dstart+4) == 0x4C))
 {
   printf("\n            Programs\n\n");
   for (cmbpgm=0; cmbpgm<maxpgm; ++cmbpgm)      /* For 50 or 100 progs    */
   {
    item = compress(&mem,143);
    printf("I%02d: ",cmbpgm);                   /* Program number         */
    for (n=0; n<10; ++n)                        /* Program name           */
     fputc(item[n],stdout);
    printf("\n");
    free(item);
   }
   Cnecin();                                    /* Wait for keypress      */
 }

 if ((*(dstart+4) == 0x50) || (*(dstart+4) == 0x48))
 {
   printf("\n            Sequences\n\n");
   for (cmbpgm=0; cmbpgm<10; ++cmbpgm)          /* For 10 sequences       */
   {
    item = compress(&mem,96);
    printf("Song %02d: ",cmbpgm);               /* Sequence number        */
    for (n=20; n<30; ++n)                       /* Sequence name          */
     fputc(item[n],stdout);
    printf("\n");
    free(item);
   }
   Cnecin();                                    /* Wait for keypress      */
 }

/* ------------- A Dump routine, currently unused -----------------------
 byts=0;                                        /* Address counter
 while (mem<head+dumpsize)
 {
  printf("%02d: ",byts);
  for (n=0; n<3; ++n)
  {
   for (x=0; x<8; ++x)
    printf("%02X",*(mem+(n*8)+x));
   printf(" ");
  }

  printf(" ");

  for (x=0; x<24; ++x)
  {
   if ((*(mem+x)>32) && (*(mem+x)<127))
    fputc(*(mem+x),stdout);
   else
    fputc('.',stdout);
  }
  printf("\n");
  mem+=24;
  byts+=24;                                     /* Address counter
 }
-------------------------------------------------------------------------- */

 free(head);                                    /* Give head... back.     */
 exit(EXIT_SUCCESS);
}
