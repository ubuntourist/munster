/************************************************************************
 *                                                                      *
 *       decypher.c             Written by Kevin Cole   09-Jan-92       *
 *                                                                      *
 *  This program attempts to decypher the contents of a Korg M1 System  *
 * Exclusive Dump All.  Eventually, it will become a subroutine of both *
 * dumper and loader, in order to provide feedback to the user as to    *
 * what is being dumped or loaded.                                      *
 *                                                                      *
 ************************************************************************/

#include <stdio.h>

/************************************************************************
 *                                                                      *
 *      expand                                                          *
 *                                                                      *
 *  Takes in the internal Korg array in 8-bit bytes, a.k.a. "true" size *
 * and returns the external (i.e. MIDI or fake) size of the array as    *
 * 7-bit bytes.                                                         *
 *                                                                      *
 ************************************************************************/

int expand(truesize)
int truesize;
{
 int fakesize;

 fakesize = (truesize / 7) * 8;                 /* Seven 8-bit to eight 7-bit */
 if ((truesize % 7) > 0)
   fakesize += (1 + (truesize % 7));            /* Take care of leftovers */
 return(fakesize);
}

main()
{
 FILE *fin;                                     /* Where to read from     */
 unsigned char buf[80],*fid;                    /* User supplied File ID  */
 unsigned char phmbuf[143],cmbbuf[124];         /* Prog & Comb buffers    */
 unsigned int x,y,group,byts,strm,maxpgm;

 x = 0; byts = 0;                               /* Initialize variables   */

 printf("Enter name of source file [RAM:m1all.dmp]: ");
 fflush(stdout);                                /* Ask it NOW!            */
 fid = "RAM:m1all.dmp";                         /* Use default name.      */
 if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%s",fid);                        /* Or, use a real name    */

 fin  = fopen(fid,"rb");                        /* Where to read from     */

 while((strm = fgetc(fin)) != EOF)              /* How big is the file?   */
 {
  ++byts;
 }
 fclose(fin);

 printf("Dump read from %s. %u bytes\n",fid,byts);      /* Print totals   */

 fin  = fopen(fid,"rb");                        /* Where to read from     */
 printf("Header: ");
 for (x=0; x<6; x++)
 {
  strm = fgetc(fin); buf[x] = strm; printf("%02x ",strm);
 }
 printf("\n");

 for (x=0; x<6; x++) printf("%02x\n",buf[x]);   /***DEBUG***/

 maxpgm = (buf[5] & 2) ? 50 : 100;              /* Highest Comb or Prog # */
 printf("%u Programs/%u Combinations\n",maxpgm,maxpgm);

 x = fgetc(fin);  x = x + (fgetc(fin)<<7);      /* Let's compute a length */
 printf("Sequencer data: %u bytes long.\n",x);

/************************************************************************
 * Got it!  The NULL byte appears as a result of bit-shuffling to turn  *
 * seven 8-bit bytes into eight 7-bit bytes.  The first byte in a group *
 * of eight contains the high-order bits from the remaining seven bytes.*
 * Tres twisted!                                                        *
 ************************************************************************/

 printf("Array expanded to: %d\n",expand(143));
 for (x=0; x<984; x++)
  strm = fgetc(fin);                            /* Ignore global data     */

 for (y=0; y<maxpgm; y++)
 {
  for (x=0; x<10; x++)
  {
  strm = fgetc(fin);  fputc(strm,stdout);       /* print combination name */
  }
  fputc('\n',stdout); fflush(stdout);
  getc(stdin);
  for (x=10; x<142; x++)
   strm = fgetc(fin);                           /* Ignore global data     */
 }
 printf("\n"); fflush(stdout);
 fclose(fin);

 exit();                                        /* Done!                  */
}
