#include <stdio.h>
#include <stdlib.h>

#define DEC(c)  (((c) - ' ') & 077)

void main()
{
  int n;
  char dest[128], a,b,c,d;

  scanf("begin %o ", &n);
  fgets(dest, 128, stdin);

  if (freopen(dest, "w", stdout) == NULL) {
    perror(dest);
    exit(EXIT_FAILURE);
  }

  while ((n=getchar()) != EOF && (n=DEC(n))!=0)  {
    while (n>0) {
      a = DEC(getchar());
      b = DEC(getchar());
      c = DEC(getchar());
      d = DEC(getchar());
      if (n-- > 0) putchar(a << 2 | b >> 4);
      if (n-- > 0) putchar(b << 4 | c >> 2);
      if (n-- > 0) putchar(c << 6 | d);
    }
    n=getchar();
  }
  exit(EXIT_SUCCESS);
}
