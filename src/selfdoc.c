/************************************************************************
 *                                                                      *
 *      selfdoc.c                                                       *
 *                                                                      *
 *   This illustrates the use of compile-time constants which get       *
 * imbedded in the resulting code.  Use this!!!                         *
 *                                                                      *
 ************************************************************************/
#include <stdio.h>

void main()
{
 printf("This file, %s,\nwas compiled on %s at %s\n",
        __FILE__,__DATE__,__TIME__);
 printf("This printf is on line %d\n",__LINE__+2);
#line 100
 printf("The pseudoconstant __LINE__ has been changed to %d\n",__LINE__);
}
