/*    readkey.c   */

#include "osbind.h"
#include <stdio.h>

delay()
{
 int d1,d2;
 for (d1=0; d1<1000; d1++)
 {
  for (d2=0; d2<10; d2++)
  {
   d2=d2;
  }
 }
}

note_on(channel,note,velocity)
int channel,note,velocity;
{
 Bconout(3,(128+16+channel));  Bconout(3,note); Bconout(3,velocity);
}

note_off(o_channel,o_note,o_velocity)
int o_channel,o_note,o_velocity;
{
 Bconout(3,(128+o_channel));  Bconout(3,o_note); Bconout(3,o_velocity);
}

int from_MIDI()
{
 int ifm; ifm = Bconin(3); ifm &= 0x00ff; return(ifm);
}

int check_MIDI()
{
 int ims; ims = Bconstat(3); if (ims!=0) ims = 1; return(ims);
}

main()
{
 int program,noteval;
 program = -1;
 while(program!=0)
 {
  if(check_MIDI()!=0)
  {
   noteval = from_MIDI();
   if (noteval<254)
   {
    printf("%d",noteval);
    noteval = from_MIDI(); printf(" %d",noteval);
    noteval = from_MIDI(); printf(" %d\n",noteval);
   }
  }
 }
}

