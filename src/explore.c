#include <stdio.h>

main(argc,argv)
int argc;  char *argv[];
{
 FILE *fout;                                    /* Where to write to      */
 FILE *fin;                                     /* Where to read from     */
 extern unsigned char *malloc();
 unsigned char *mem,*head,*tail;                /* Pointers to characters */
 unsigned int dmptyp,x,byts,strm,success;       /* Miscellaneous stuff    */
 unsigned char typ[5],buf[80],fid[80];          /* User supplied File ID  */
 unsigned char reqst[7] = {0xF0,0x42,0x30,0x19,0x0F,0x00,0xF7};
 unsigned char rspns[7] = {0xF0,0x42,0x30,0x19,0x50,0x00,0xF7};

 mem = NULL; head = NULL; tail = NULL;          /* Initialize all pointers  */

 if (argc == 2)                                 /* Name on command line     */
 {
  strcpy(fid,argv[1]);
  printf("Loading Korg from %s.\n",fid);
  fflush(stdout);
 }
 else
 {
  strcpy(fid,"RAM:m1all.dmp");                  /* Use default name.        */
  printf("Enter name of source file [%s]: ",fid);
  fflush(stdout);                               /* Ask it NOW!              */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%s",fid);                        /* Or, use a real name      */
 }

 while ((fin = fopen(fid,"rb")) == NULL)        /* Non-existent file        */
 {
  printf("File %s does not exist.  Quit? [N]: ",fid);
  fflush(stdout);                               /* Ask it NOW!              */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   if ((buf[0] & 0x5F) == 'Y')                  /* What's the answer?       */
    exit();
   else
   {
     printf("Enter name of source file [%s]: ",fid);
     fflush(stdout);                            /* Ask it NOW!              */
     if (fgets(buf,sizeof(buf),stdin) != NULL)
      sscanf(buf,"%s",fid);                     /* Or, use a real name      */
   }
 }

 x = 0; byts = 0;                               /* Initialize variables   */
 if ((head = malloc(60000L)) == NULL)           /* Give me LOTS of memory */
 {
   printf("The memory allocation failed\n");
   exit();
 }

#ifdef VAX
 memset(head,0,64000);                          /* Zero the memory we got */
#else
 setmem(head,64000,0);                          /* Zero the memory we got */
#endif
 tail = head;                                   /* Point to the start     */

 while((*tail = (char) fgetc(fin) & 0x00FF) != EOF)     /* Get a byte       */
 {
   printf("%04X : %02x\n",tail,*tail);                  /***DEBUG***/
   ++tail;
 }

 fclose(fin);
 byts = tail - head;                            /* How long has it been?  */
 printf("  So far, so good.\n");
 printf("Korg dump (programs, combinations and sequences) now in memory.\n");
 printf("%u bytes temporarily used.\n",byts);   /* 33 days, sir.  */

 for(mem=head; mem<tail; ++mem)                 /* Search all used memory */
 {
  success = 0;                                  /* Assume no match found  */
  for(x=0; x<4; ++x)                            /* Compare 6-byte window  */
  {
   if(*(mem+x) != rspns[x])                     /* Contents of (mem+x)  */
     success = 0;
   else
     ++success;
  }
  if (success == 4) break;                      /* Entirely successful?   */
 }

 if (success != 4)                              /* Couldn't find 6 bytes  */
 {
  printf("Something failed... No Dump header found.\n");
  exit();
 }

 switch (*(mem+4))
 {
  case 0x48:
    printf("Loading All Sequences\n");                           break;
  case 0x4C:
    printf("Loading All Programs\n");                            break;
  case 0x4D:
    printf("Loading All Combinations\n");                        break;
  case 0x51:
    printf("Loading All Global Data\n");                         break;
  case 0x50:
    printf("Loading Everything (Global, Prog, Comb, & Seq)\n");  break;
  default:
    printf("This is not a Korg M1 dump.\n");                     break;
 }

 byts = 0;
 while(mem<tail)                                /* Starting with header   */
 {
/*  fputc(*mem,fout);                           / Write out the file      */
  ++mem;                                        /* and count bytes.       */
  ++byts;
 }

 free(head);                                    /* Give head... back.     */

 printf("Actual dump is %u bytes\n",byts);      /* Print totals */
 return(0);
}
