/************************************************************************
 *                                                                      *
 *       dumper.c  v0.3         Written by Kevin Cole   04-Jan-92       *
 *                              Last revised:           02-Mar-92       *
 *                                                                      *
 *  This program is based on dumper.c.  It's supposed to stuff a MIDI   *
 * dump into an Atari file...  The original tried to initiate the dump  *
 * and had problems.  This version waits for dump to be initiated from  *
 * the Korg M1 keyboard.                                                *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.02.06.01  KJC  Combined filer.c and dumper.c.                *
 *      92.03.02.01  KJC  Changed fid from a pointer to an array, due   *
 *                        to technical difficulties in another program. *
 *                                                                      *
 ************************************************************************/

#include "osbind.h"
#include <stdio.h>
#include "korg.h"

int gflag;                                      /* global flag */

from_MIDI()
{
 int ifm;  ifm = Bconin(3) & 0x00FF;  return(ifm);
}

MIDI_status()
{
 int ims;  ims = Bconin(3); if(ims != 0) ims = 1; return(ims);
}

check_MIDI()
{
 int flag,ret;
 flag = MIDI_status();
 switch(flag)
 {
  case 0:
    gflag = 1;
    break;
  default:
    ret = from_MIDI();
    break;
 }
 return(ret);
}

flush_MIDI()
{
 gflag = 0; while (gflag = 0) check_MIDI();
}

main()
{
 FILE *fout;                                    /* Where to write to  */
 FILE *fin;                                     /* Where to read from */
 char buf[80],fid[80];                          /* User supplied File ID */
 unsigned int x,byts,eoxs,strm;
 unsigned char reqst[7] = {0xF0,0x42,0x30,0x19,0x0F,0x00,0xF7};

 printf("             Korg M1 MIDI Dump  v0.3  by Kevin Cole  07-Feb-92\n\n");
 printf(" This program saves program, combination and sequence data in the ");
 printf(" file of\n your choice.  Make sure that your destination disk has ");
 printf(" at least 55000 bytes\n available.\n\n");
 printf("             Type any character to begin dump.");
 fflush(stdout);                                /* Ask it NOW! */
 x = getc(stdin);                               /* Wait for keystroke */
 printf("\n\nPlease wait...");  fflush(stdout);

 x = 0; byts = 0; eoxs = 0;                     /* Initialize variables */
 flush_MIDI();                                  /* Start clean */
 fout = fopen("e:m1all.dmp","wb");              /* Where to dump to */

 for(x=0; x<7; x++)
  Bconout(3,reqst[x]);                          /* Request Dump All */

 while (x == x)                                 /* Spin wheels */
 {
  strm = Bconin(3) & 0x00FF;                    /* Ignore a masked MIDI byte */
  if (strm == 0xF0)                             /* If we get a SYS EX... */
  {
   fputc(strm,fout); ++byts;  break;            /* Save it, count it, & exit */
  }
 }

 while (x == x)                                 /* Ad nauseum */
 {
  strm = Bconin(3) & 0x00FF;                    /* Get a masked MIDI byte */
  fputc(strm,fout);                             /* Write it to file */
  ++byts;                                       /* Count bytes received */
  if (strm == 0xFE) break;                      /* Break on active sensing */
 }

 fclose(fout);

 printf("  So far, so good.\n");
 printf("Korg dump (programs, combinations and sequences) now in memory.\n");
 printf("Enter name of destination file [d:m1all.dmp]: ");
 fflush(stdout);                                /* Ask it NOW! */
 strcpy(fid,"d:m1all.dmp");                     /* Use default name.    */
 if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%s",fid);                        /* Or, use a real name  */
 fin  = fopen("e:m1all.dmp","rb");              /* Where to read from   */
 fout = fopen(fid,"wb");                        /* Where to dump to     */

 while((strm = fgetc(fin)) != EOF)              /* REALLY save the file */
 {
  fputc(strm,fout);
 }
 fclose(fout);
 fclose(fin);

 printf("Dump saved to %s. %u bytes\n",fid,byts);       /* Print totals */
 exit();
}
