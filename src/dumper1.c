/************************************************************************
 *                                                                      *
 *      dumper.c  v1.0          Written by Kevin Cole   04-Jan-92       *
 *                              Last revised:           02-Mar-92       *
 *                                                                      *
 *  This program is based on dumper.c.  It's supposed to stuff a MIDI   *
 * dump into an Atari file...  The original tried to initiate the dump  *
 * and had problems.  This version waits for dump to be initiated from  *
 * the Korg M1 keyboard.                                                *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.02.06.01  KJC  Combined filer.c and dumper.c.                *
 *      92.02.18.01  KJC  Added GEM support to keep user from shell.    *
 *      92.02.18.02  KJC  Removed GEM support cuz it don't work.        *
 *      92.02.25.01  KJC  Supports filename on command line now.        *
 *      92.02.25.02  KJC  Checks for existing file before overwriting.  *
 *      92.02.29.01  KJC  Added a check for bad non-dump data.          *
 *      92.03.02.01  KJC  Changed fid from a pointer to an array, due   *
 *                        to technical difficulties in another program. *
 *                                                                      *
 ************************************************************************/

#include "osbind.h"
#include <stdio.h>
#include "korg.h"

/* GEM Iniitialization stuff.  Pre-slug the intin array, get a "handle" *
 * and open a virtual workstation.... whatever that means.              */

/*
int intin[256],intout[256],ptsin[256],ptsout[256],contrl[12],handle,dummy;
init_GEM()
{
 int i;
 for (i=0; i<10; i++) intin[i] = 1; intin[10] = 2;
 handle = graf_handle(&dummy,&dummy,&dummy,&dummy);
 v_opnvwk(intin,&handle,intout);
}
*/

int gflag;                                      /* global flag */

from_MIDI()
{
 int ifm;  ifm = Bconin(3) & 0x00FF;  return(ifm);
}

MIDI_status()
{
 int ims;  ims = Bconin(3); if(ims != 0) ims = 1; return(ims);
}

check_MIDI()
{
 int flag,ret;
 flag = MIDI_status();
 switch(flag)
 {
  case 0:
    gflag = 1;
    break;
  default:
    ret = from_MIDI();
    break;
 }
 return(ret);
}

flush_MIDI()
{
 gflag = 0; while (gflag = 0) check_MIDI();
}

main(argc,argv)
int argc;  char *argv[];
{
 FILE *fout;                                    /* Where to write to      */
 FILE *fin;                                     /* Where to read from     */
 char buf[80],fid[80];                          /* User supplied File ID  */
 unsigned int x,byts,eoxs,strm;
 unsigned char reqst[7] = {0xF0,0x42,0x30,0x19,0x0F,0x00,0xF7};
 unsigned char rspns[7] = {0xF0,0x42,0x30,0x19,0x50,0x00,0xF7};

/* appl_init();  init_GEM();  v_clrwk(handle);  / Init GEM and clear screen */

 printf("             Korg M1 MIDI Dump  v1.0  by Kevin Cole  25-Feb-92\n\n");
 printf(" This program saves program, combination and sequence data in the ");
 printf(" file of\n your choice.  Make sure that your destination disk has ");
 printf(" at least 55000 bytes\n available.\n\n");
 printf("             Type any character to begin dump.");
 fflush(stdout);                                /* Ask it NOW!            */
 x = getc(stdin);                               /* Wait for keystroke     */
 printf("\n\nPlease wait...");  fflush(stdout);

 x = 0; byts = 0; eoxs = 0;                     /* Initialize variables   */
 flush_MIDI();                                  /* Start clean            */
 fout = fopen("e:m1all.dmp","wb");              /* Where to dump to       */

 for(x=0; x<7; x++)
  Bconout(3,reqst[x]);                          /* Request Dump All       */

 while (x == x)                                 /* Spin wheels            */
 {
  strm = Bconin(3) & 0x00FF;                    /* Ignore a masked MIDI byte */
  if (strm == 0xF0)                             /* If we get a SYS EX...  */
  {
   fputc(strm,fout); ++byts;  break;            /* Save it, count it, & exit */
  }
 }

 while (x == x)                                 /* Ad nauseum             */
 {
  strm = Bconin(3) & 0x00FF;                    /* Get a masked MIDI byte */
  fputc(strm,fout);                             /* Write it to file       */
  ++byts;                                       /* Count bytes received   */
  if (strm == 0xFE) break;                      /* Break on active sensing */
 }

 fclose(fout);

 printf("  So far, so good.\n");
 printf("Korg dump (programs, combinations and sequences) now in memory.\n");

 if (argc == 2)                                 /* Name on command line   */
 {
  strcpy(fid,argv[1]);
  printf("Dump will be saved in %s.\n",fid);
  fflush(stdout);
 }
 else
 {
  strcpy(fid,"d:m1all.dmp");                    /* Use default name.      */
  printf("Enter name of destination file [%s]: ",fid);
  fflush(stdout);                               /* Ask it NOW!            */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%s",fid);                        /* Or, use a real name    */
 }

 while ((fin = fopen(fid,"rb")) != NULL)        /* Prevent overwriting    */
 {
  fclose(fin);
  printf("File %s already exists.  Overwrite it? [N]: ",fid);
  fflush(stdout);                               /* Ask it NOW!            */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   if ((buf[0] & 0x5F)== 'Y')                   /* What's the answer?     */
    break;
   else
   {
     printf("Enter name of destination file [%s]: ",fid);
     fflush(stdout);                            /* Ask it NOW!            */
     if (fgets(buf,sizeof(buf),stdin) != NULL)
      sscanf(buf,"%s",fid);                     /* Or, use a real name    */
   }
 }
 fin  = fopen("e:m1all.dmp","rb");              /* Where to read from     */
 fout = fopen(fid,"wb");                        /* Where to dump to       */

 byts = 0;                                      /* Point to first element */
 while((strm = fgetc(fin)) != EOF)              /* REALLY save the file   */
 {
  if(byts < 6)                                  /* Examine first 6 bytes  */
  {
   if(strm == rspns[byts])                      /* Is it the real header? */
   {
    fputc(strm,fout);  byts++;                  /* If so, save it.        */
   }
  }
  else
  {
   fputc(strm,fout);  byts++;                   /* Past header. Save it.  */
   if (strm == rspns[6]) break;                 /* Stop after writing EOX */
  }
 }

 fclose(fout);
 fclose(fin);

 printf("Dump saved to %s. %u bytes\n",fid,byts);       /* Print totals */
/* appl_exit();                                 / Exit GEM nicely */
 exit();
}
