/* Verbatim from p. 322 of the Mark Williams C book, an example of menus */

#include <aesbind.h>
#include <obdefs.h>
#include <gemdefs.h>

#define SPEC 0x111C1L
/*
 * It's a bunch of flags:
 *    1 << 16    = Border, 1 raster thick
 *    1 << 12    = Fill Color  (1 = black)
 *    1 <<  8    = Text Color  (1 = black)
 *    1 <<  7    = Turn on replace bit
 *    4 <<  4    = Fill pattern to grey
 *    1          = Border color
 */

#define COLOR 0x010F0L
/*
 * Flags again:
 *    0 << 16    = Border thickness 0 rasters
 *    1 << 12    = Fill color
 *    0 <<  8    = Text color  (0 = white)
 *    1 <<  7    = Turn on replace bit
 *    7 <<  4    = Fill pattern to solid
 *    0          = Border color
 */

/* Strings used in the menu object */
char desk[] = "  Desk";
char quit[] = "  Quit";
char line[] = " ------------------ ";
char blank[] = "";

/* Define an object that masks the screen */
OBJECT mask[] = {
/*  N /  H /  T /  type / flags / state / specs / X / Y /  W /   H   */
   -1,  -1,  -1,  G_BOX, LASTOB, NORMAL, SPEC,    0,  0, 639,  399
};

