/************************************************************************
 *                                                                      *
 *      stripper.c   v0.5       Written by Kevin Cole   29-Feb-92       *
 *                              Last revised:           02-Mar-92       *
 *                                                                      *
 *   This filters the crap out of old Korg M1 MIDI dumps produced with  *
 * the original dumper program.  It searches for the M1 Dump header and *
 * ignores any data in the file prior to that point.  This was written  *
 * after I found a dumper file that caused loader to bomb out and the   *
 * M1 to freeze.  Examination of the file with the symbolic debugger db *
 * and the octal dump program od, revealed a file that started with a   *
 * mode change, followed by sequencer data, before the actual dump data.*
 * Stripping off this silly stuff left me with a working dump file.     *
 *                                                                      *
 * The latest version of dumper handles this automatically.  This is    *
 * only for files produced prior to 28-Feb-92.                          *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.03.02.01  KJC  Pointers fid1 & fid2 initially pointed to     *
 *                        command line arguments.  Lengthening the file-*
 *                        name pointed at by fid1, without adjusting    *
 *                        pointers caused the string for fid1 to extend *
 *                        into fid2's space, thus overwriting the file- *
 *                        name for fid2.  This has been fixed by chang- *
 *                        ing the fid pointers to arrays.               *
 *                                                                      *
 ************************************************************************/

#include "osbind.h"
#include <stdio.h>
#include "korg.h"

main(argc,argv)
int argc;  char *argv[];
{
 FILE *fout;                                    /* Where to write to      */
 FILE *fin;                                     /* Where to read from     */
 char buf[80],fid1[80],fid2[80];                /* User supplied File ID  */
 unsigned int byts,strm;
 unsigned char rspns[7] = {0xF0,0x42,0x30,0x19,0x50,0x00,0xF7};

 if (argc == 3)                                 /* Names on command line  */
 {
  strcpy(fid1,argv[1]);
  strcpy(fid2,argv[2]);
  printf("Dump will be read from %s and saved in %s.\n",fid1,fid2);
  fflush(stdout);
 }
 else
 {
  strcpy(fid1,"d:m1all.dmp");                   /* Use default name.        */
  printf("Enter name of source file [%s]: ",fid1);
  fflush(stdout);                               /* Ask it NOW!              */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%s",fid1);                       /* Or, use a real name      */

  strcpy(fid2,"d:m1all.dmp");                   /* Use default name.        */
  printf("Enter name of destination file [%s]: ",fid2);
  fflush(stdout);                               /* Ask it NOW!              */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%s",fid2);                       /* Or, use a real name      */
 }

 while ((fin = fopen(fid2,"rb")) != NULL)       /* Prevent overwriting      */
 {
  fclose(fin);
  printf("File %s already exists.  Overwrite it? [N]: ",fid2);
  fflush(stdout);                               /* Ask it NOW!              */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   if ((buf[0] & 0x5F) == 'Y')                  /* What's the answer?       */
    break;
   else
   {
     printf("Enter name of destination file [%s]: ",fid2);
     fflush(stdout);                            /* Ask it NOW!              */
     if (fgets(buf,sizeof(buf),stdin) != NULL)
      sscanf(buf,"%s",fid2);                    /* Or, use a real name      */
   }
 }

 while ((fin = fopen(fid1,"rb")) == NULL)       /* Non-existent file check  */
 {
  printf("File %s does not exist.  Quit? [N]: ",fid1);
  fflush(stdout);                               /* Ask it NOW!              */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   if ((buf[0] & 0x5F) == 'Y')                  /* What's the answer?       */
    exit();
   else
   {
     printf("Enter name of source file [%s]: ",fid1);
     fflush(stdout);                            /* Ask it NOW!              */
     if (fgets(buf,sizeof(buf),stdin) != NULL)
      sscanf(buf,"%s",fid1);                    /* Or, use a real name      */
   }
 }

 fout = fopen(fid2,"wb");                       /* Actually open out file */
 byts = 0;                                      /* Point to first element */

 while((strm = fgetc(fin)) != EOF)              /* REALLY save the file   */
 {
  if(byts < 6)                                  /* Examine first 6 bytes  */
  {
   if(strm == rspns[byts])                      /* Is it the real header? */
   {
    fputc(strm,fout);  byts++;                  /* If so, save it.        */
   }
  }
  else
  {
   fputc(strm,fout);  byts++;                   /* Past header. Save it.  */
   if (strm == rspns[6]) break;                 /* Stop after writing EOX */
  }
 }

 fclose(fout);
 fclose(fin);

 printf("Dump saved. %u bytes\n",byts);         /* Print totals */
 exit();
}
