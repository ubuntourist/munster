/************************************************************************
 *                                                                      *
 *      search.c                Written by Kevin Cole   07-Mar-92       *
 *                              Last Modified:          07-Mar-92       *
 *                                                                      *
 *  This eventual subroutine is designed to slide a window through mem- *
 * ory searching for a specified string.  I guess the logical thing to  *
 * do would be to either return a pointer or an offset to the start of  *
 * the found string.  This version is in development, i.e. *NOT* ready  *
 * for prime time.                                                      *
 *                                                                      *
 ************************************************************************/

#include <stdio.h>
#define TRUE    -1
#define FALSE    0

main(argc,argv)
int argc;  char *argv[];
{
 FILE *fout;                                    /* Where to write to      */
 FILE *fin;                                     /* Where to read from     */
 extern unsigned char *malloc();
 unsigned char *mem,*head,*tail;                /* Pointers to characters */
 unsigned int dmptyp,x,byts,strm,success;       /* Miscellaneous stuff    */
 unsigned char typ[5],buf[80],fid[80];          /* User supplied File ID  */
 unsigned char reqst[7] = {0xF0,0x42,0x30,0x19,0x0F,0x00,0xF7};
 unsigned char rspns[7] = {0xF0,0x42,0x30,0x19,0x50,0x00,0xF7};

 fin  = fopen("RAM:qwert","rb");                /* Where to read from     */
 fout = fopen("RAM:qwert.2","wb");              /* Where to write to      */
 if ((head = malloc(64000L)) == NULL)           /* Give me LOTS of memory */
 {
   printf("The memory allocation failed\n");
   exit();
 }

 setmem(head,64000,0);                          /* Zero the memory we got */
 tail = head;                                   /* Point to the start     */

 while((*tail = (char) fgetc(fin) & 0x00FF) != EOF)     /* Til EOF do...  */
  ++tail;

 byts = tail - head;                            /* How long has it been?  */
 printf("%d bytes used.",byts);                 /* 33 days, sir.          */

 for(mem=head; mem<tail; ++mem)                 /* All available memory   */
 {
  success = FALSE;                              /* Assume no match found  */
  for(x=0; x<6; ++x)                            /* Compare 6-byte window  */
  {
   if(*(mem+x) != rspns[x])                     /* Contents of (mem+x)  */
     success = FALSE;
   else
     success = TRUE;
  }
  if (success) break;                           /* Entirely successful?   */
 }

 byts = 0;
 while(mem<tail)
 {
  fputc(*mem,fout);
  ++mem;
  ++byts;
 }

 free(head);                                    /* Give head... back.     */
 fclose(fout);
 fclose(fin);

 printf("Dump saved. %u bytes\n",byts);         /* Print totals   */
 exit();
}

