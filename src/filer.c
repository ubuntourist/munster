/************************************************************************
 *                                                                      *
 *       filer.c                Written by Kevin Cole   06-Jan-92       *
 *                                                                      *
 *  This program is based on dumper.c.  It's supposed to stuff a MIDI   *
 * dump into an Atari file...  The original tried to initiate the dump  *
 * and had problems.  This version waits for dump to be initiated from  *
 * the Korg M1 keyboard.                                                *
 *                                                                      *
 ************************************************************************/

#include "osbind.h"
#include <stdio.h>

int gflag;                                      /* global flag */

from_MIDI()
{
 int ifm;  ifm = Bconin(3) & 0x00FF;  return(ifm);
}

MIDI_status()
{
 int ims;  ims = Bconin(3); if(ims != 0) ims = 1; return(ims);
}

check_MIDI()
{
 int flag,ret;
 flag = MIDI_status();
 switch(flag)
 {
  case 0:
    gflag = 1;
    break;
  default:
    ret = from_MIDI();
    break;
 }
 return(ret);
}

flush_MIDI()
{
 gflag = 0; while (gflag = 0) check_MIDI();
}

main()
{
FILE *fout;
unsigned int x,byts,eoxs,strm;

 x = 0; byts = 0; eoxs = 0;                     /* Initialize variables */
 flush_MIDI();                                  /* Start clean */
 fout = fopen("e:m1all.dmp","wb");              /* Where to dump to */

 while (x == x)                                 /* Spin wheels */
 {
  strm = Bconin(3) & 0x00FF;                    /* Ignore a masked MIDI byte */
  if (strm == 0xF0)                             /* If we get a SYS EX... */
  {
   fputc(strm,fout); ++byts;  break;            /* Save it, count it, & exit */
  }
 }

 while (x == x)                                 /* Ad nauseum */
 {
  strm = Bconin(3) & 0x00FF;                    /* Get a masked MIDI byte */
  fputc(strm,fout);                             /* Write it to file */
  ++byts;                                       /* Count bytes received */
  if (strm == 0xFE) break;                      /* Break on active sensing */
 }

 printf("Dump saved to E:M1ALL.DMP. %u bytes\n",byts);  /* Print totals */
 fclose(fout);
 exit();
}
