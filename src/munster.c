/************************************************************************
 *                                                                      *
 *      munster.c  v2.0         Written by Kevin Cole   04-Apr-92       *
 *                              Last Revised:           12-Dec-92       *
 *                                                                      *
 *      *** NOT TESTED YET ***  USE MONSTER.C UNTIL THEN ***            *
 *                                                                      *
 *   Really, nothing more than a combination of dumper and loader,      *
 * along with some stuff to analyze the contents of the dump file.      *
 *                                                                      *
 *   IMPORTANT: The Atari is sensitive to the file extension.  In order *
 * for this to run correctly from both the desktop and msh, the file    *
 * extension must be either .TPP or .TOS.  So, complie this on the      *
 * Atari with the command:                                              *
 *                                                                      *
 *              cc -o monster.tos  monster.c                            *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.04.14.01  KJC  Temporarily changed MIDI global channel.      *
 *      92.04.23.01  KJC  Removed GEM stuff...                          *
 *      92.11.30.01  KJC  Added function prototype definitions for ANSI *
 *                        standards compatiblity, and rearranged the    *
 *                        order of the functions.                       *
 *      92.12.12.01  KJC  More modularity: moved code, creating two new *
 *                        functions: FindDump and RollCall.             *
 *                                                                      *
 ************************************************************************/

#include <stdio.h>
#include "osbind.h"
#include "korg.h"

                /* Functions */

void main();                            /* Obvious, n'est pas?          */
int dumper();                           /* Dumps from Korg M1 to disk   */
int loader();                           /* Loads from disk to Korg M1   */
int sevn2ate();                         /* Size for 7 to 8 conversions  */
unsigned char *compress();              /* Converts from 7-bit to 8-bit */
unsigned char *FindDump();              /* Search memory for dump start */
unsigned char *RollCall();              /* Prints item number & name    */
extern unsigned char *lcalloc();        /* Allocates gobs of memory     */

                /* Variables */

unsigned char buf[80],fid[80],mode,memfmt;
unsigned char modrq[7] = {0xF0,0x42,0x3F,0x19, 0x12,            0xF7,0xFF};
unsigned char modch[9] = {0xF0,0x42,0x3F,0x19, 0x4E,0x04,0x10,  0xF7,0xFF};
unsigned char reqst[8] = {0xF0,0x42,0x3F,0x19, 0x0F,0x00,       0xF7,0xFF};
unsigned char rspns[8] = {0xF0,0x42,0x3F,0x19, 0x50,0x00,       0xF7,0xFF};
unsigned int  x;

/************************************************************************
 *                                                                      *
 *      main                                                            *
 *                                                                      *
 *   All this does is drive the other parts (menu, dumper, & loader).   *
 *                                                                      *
 ************************************************************************/

void main(argc,argv)
int argc;  char *argv[];
{
 void startup();
 void closedown();
 void menu();

 fid[0] = '\0';                                 /* Filename is empty...   */
 if (argc == 2)                                 /* ...unless supplied...  */
   strcpy(fid,argv[1]);                         /* ...on command line.    */
 startup();                                     /* GEM or non-GEM start   */
 menu();                                        /* Live in the menu       */
 closedown();                                   /* Cleanup & Closedown    */
}

/************************************************************************
 *                                                                      *
 *      startup                 Written by Kevin Cole   Spring 1992     *
 *                                                                      *
 *  This routine prints out a heading.  It also changes to the dump     *
 * subdirectory (D:\KORG\DUMPS).                                        *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.04.21.01  KJC  Added a check for initial mode, then set to   *
 *                        Global mode.                                  *
 *                                                                      *
 ************************************************************************/

void startup()
{
 unsigned int  byts;
 unsigned char strm;

 Dsetdrv(3);                                    /* Set drive to D:        */
 Dsetpath("\\korg\\dumps");                     /* CD to subdirectory     */
 printf("\n\n");                                /* ANSI Screen Clear      */
 printf("             Korg M1 MIDI Dump  v2.0  by Kevin Cole  08-Mar-92\n\n");
 printf(" Make sure that your destination disk has at least 56000 bytes ");
 printf("available for\n a full dump.  Less for the others.\n\n");
 for(x=0; x<6; x++)
  Bconout(3,modrq[x]);                          /* Make a Mode Inquiry      */

 byts = 0;
 while (Bconstat(3) != 0)                       /* Get the entire message.  */
 {
  strm = (unsigned char) (Bconin(3) & 0x00FF);  /* Get a masked MIDI byte   */
  if (strm != 0xFE)                             /* If not Active Sensing... */
   buf[byts++] = strm;                          /* ...save it & count it.   */
  if ((strm == 0xFE) && (byts != 0)) break;     /* Break on active sensing. */
 }
 mode = buf[5];                                 /* Save current mode        */
 memfmt = buf[6];                               /* Save memory allocation   */

 return;
}

/************************************************************************
 *                                                                      *
 *      menu                    Written by Kevin Cole   Spring 1992     *
 *                                                                      *
 *   This routine just asks the user for a choice, ad nauseum.          *
 *                                                                      *
 ************************************************************************/

void menu()
{
 int dmptyp;

 dmptyp = 5;                                    /* Default to dump ALL    */
 for (;;)                                       /* For ever do            */
 {
  printf(" Choose one of the following:\n\n");
  printf("    1 = Dump All Sequences       ( 2000 to 38000 bytes)\n");
  printf("    2 = Dump All Programs        ( 8179 or 16350 bytes)\n");
  printf("    3 = Dump All Combinations    ( 7093 or 14179 bytes)\n");
  printf("    4 = Dump All Global Data     (  991 bytes)\n");
  printf("    5 = Dump Everything          (18000 to 55000 bytes)\n");
  printf("    6 = Load a file\n");
  printf("    9 = Exit\n");

  printf("\nDump type (1-6) [%d]: ",dmptyp);    /* Choose a dump type     */
  fflush(stdout);                               /* Ask it NOW!            */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
    sscanf(buf,"%d",&dmptyp);                   /* <-- Remember '&' trick */

  switch (dmptyp)
  {
   case 1:
     while (dumper(0x18,0x48,".seq") == 0) ;    break;
   case 2:
     while (dumper(0x1C,0x4C,".pgm") == 0) ;    break;
   case 3:
     while (dumper(0x1D,0x4D,".cmb") == 0) ;    break;
   case 4:
     while (dumper(0x0E,0x51,".gbl") == 0) ;    break;
   case 5:
     while (dumper(0x0F,0x50,".all") == 0) ;    break;
   case 6:
     while (loader() == 0) ;                    break;
   case 9:
     return;
   default:
     printf("That wasn't an option.  Try again.\n\n");
     break;
  }
 }
}

/************************************************************************
 *                                                                      *
 *      closedown               Written by Kevin Cole   Spring 1992     *
 *                                                                      *
 *  This routine resets the Korg to normal.  It also changes to the     *
 * subdirectory D:\KORG.                                                *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.04.21.01  KJC  Added code to restore initial mode.           *
 *                                                                      *
 ************************************************************************/

void closedown()
{
 Dsetpath("\\korg");                            /* CD back to start       */
 modch[5] = mode;                               /* Restore saved mode     */
 for(x=0; x<8; x++)
  Bconout(3,modch[x]);                          /* Return to initial mode */
 printf("Hit any key to exit.");
 fflush(stdout);                                /* Ask it NOW!            */
 Cnecin();                                      /* Getchar, no echo       */
 printf("\n");
 exit();                                        /* Yes, exit normally     */
}

/************************************************************************
 *                                                                      *
 *      dumper.c  v2.0          Written by Kevin Cole   04-Jan-92       *
 *                              Last revised:           13-Apr-92       *
 *                                                                      *
 *  This program dumps Korg M1 MIDI data to the Atari 1040 ST.          *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.02.06.01  KJC  Combined filer.c and dumper.c.                *
 *      92.02.18.01  KJC  Added GEM support to keep user from shell.    *
 *      92.02.18.02  KJC  Removed GEM support cuz it don't work.        *
 *      92.02.25.01  KJC  Supports filename on command line now.        *
 *      92.02.25.02  KJC  Checks for existing file before overwriting.  *
 *      92.02.29.01  KJC  Added a check for bad non-dump data.          *
 *      92.03.02.01  KJC  Changed fid from a pointer to an array, due   *
 *                        to technical difficulties in another program. *
 *      92.03.02.02  KJC  Use a huge array, instead of the RAM disk.    *
 *      92.03.02.03  KJC  Add different types of dumps.                 *
 *      92.03.03.02  KJC  Removed 92.03.02.02.  The Atari can't handle  *
 *                        arrays that large ??? (56000 bytes).          *
 *      92.03.04.01  KJC  Changed some of the while loops to improve    *
 *                        speed... I hope.                              *
 *      92.03.08.01  KJC  Finally! I got malloc to work.  Replaced RAM  *
 *                        disk e: with allocated memory... sort of.     *
 *      92.03.08.02  KJC  Implemented a sliding window search for the   *
 *                        start of the dump.  Together with the prior   *
 *                        patch, it should get a complete dump EVERY    *
 *                        time.                                         *
 *      92.03.20.01  KJC  The Atari doesn't like memset/setmem.  So     *
 *                        memory must be initialized when allocated.    *
 *                        Switched from malloc to calloc.               *
 *      92.03.24.01  KJC  Voodoo code.  60000 is less than 64K, but     *
 *                        calloc screwed everything up.  lcalloc used.  *
 *      92.03.24.01  KJC  Added a MODE CHANGE GLOBAL command.  The dump *
 *                        seems to work better if the mode is global.   *
 *      92.03.31.01  KJC  Now the program chooses its own directory...  *
 *      92.04.13.01  KJC  Incorporate it as a function into monster.    *
 *                                                                      *
 ************************************************************************/

int dumper(reqtyp,rsptyp,fidtyp)
unsigned char reqtyp,rsptyp,*fidtyp;
{
 FILE *fin, *fout;                              /* In and Out files       */
 unsigned char *mem,*head,*tail;                /* Pointers to characters */
 unsigned int  byts;                            /* Miscellaneous stuff    */

 reqst[4] = reqtyp;                             /* Build the request type */
 rspns[4] = rsptyp;                             /* Build the respons type */

 printf("\n             Hit any key to begin dump.");
 fflush(stdout);                                /* Ask it NOW!            */
 Cnecin();                                      /* Getchar, no echo       */
 printf("\n\nPlease wait...");  fflush(stdout);

 x = 0; byts = 0;                               /* Initialize variables   */
 head = NULL;                                   /* "Break" it first       */
 if ((head = lcalloc(60000L,1L)) == NULL)       /* Get LOTS of memory     */
 {
   printf("The memory allocation failed... Aborting.\n");
   return 0;                                    /* Return a failure code  */
 }

 while (Bconstat(3) != 0)                       /* Anything in the queue? */
  Bconin(3);                                    /* Ignore and flush it.   */

 for(x=0; x<8; x++)
  Bconout(3,modch[x]);                          /* Switch to Global Mode  */

 tail = head;                                   /* Point to the start     */
 while((*tail++ = (unsigned char) Bconin(3) & 0x00FF) != 0xF7)  
   ;                                            /* Skim for EOX           */

 for(x=0; x<7; x++)
  Bconout(3,reqst[x]);                          /* Request Dump           */

 tail = head;                                   /* Point to the start     */
 while((*tail++ = (unsigned char) Bconin(3) & 0x00FF) != 0xF7)
   ;                                            /* Skim for EOX           */

 byts = (tail - head) + 1;                      /* How long has it been?  */
 printf("  So far, so good.\n");
 printf("Korg dump (programs, combinations and sequences) now in memory.\n");
 printf("%u bytes temporarily used.\n",byts);   /* 33 days, sir.  */

 if ((mem = FindDump(head,tail,5)) == NULL)     /* Find start of dump     */
 {
  free(head);                                   /* Not found.  Die.       */
  return 0;
 }

 if (fid[0] != '\0')                            /* Name on command line   */
 {
  printf("Dump will be saved in %s.\n",fid);
  fflush(stdout);
 }
 else
 {
  strcpy(fid,"d:m1dump");  strcat(fid,fidtyp);  /* Use default name.      */
  printf("Enter name of destination file ending with %s [%s]: ",fidtyp,fid);
  fflush(stdout);                               /* Ask it NOW!            */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%s",fid);                        /* Or, use a real name    */
 }

 while ((fin = fopen(fid,"rb")) != NULL)        /* Prevent overwriting    */
 {
  fclose(fin);
  printf("File %s already exists.  Overwrite it? [N]: ",fid);
  fflush(stdout);                               /* Ask it NOW!            */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   if ((buf[0] & 0x5F)== 'Y')                   /* What's the answer?     */
    break;
   else
   {
     printf("Enter name of destination file [%s]: ",fid);
     fflush(stdout);                            /* Ask it NOW!            */
     if (fgets(buf,sizeof(buf),stdin) != NULL)
      sscanf(buf,"%s",fid);                     /* Or, use a real name    */
   }
 }
 fout = fopen(fid,"wb");                        /* Where to dump to       */

 byts = 0;
 while(mem<=tail)                               /* Starting with header   */
 {
  fputc(*mem++,fout);                           /* Write out the file     */
  ++byts;                                       /* and count bytes.       */
 }

 free(head);                                    /* Give head... back.     */
 fclose(fout);

 printf("Dump saved to %s. %u bytes\n",fid,byts);       /* Print totals   */
 printf("Verifying...\n");  fflush(stdout);
 return loader();                               /* Verify and return code */
}

/************************************************************************
 *                                                                      *
 *       loader.c               Written by Kevin Cole   30-Jan-92       *
 *                              Last revised:           13-Apr-92       *
 *                                                                      *
 *  This program is based on reader.c.  It's supposed to load a MIDI    *
 * dump back to a Korg M1...                                            *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.02.15.01  KJC  Added GEM support for user-friendliness.      *
 *      92.02.15.01  KJC  Removed GEM support cuz it don't work.        *
 *      92.02.25.01  KJC  Supports filename on command line.            *
 *      92.02.25.02  KJC  Checks for non-existent file.                 *
 *      92.02.25.03  KJC  Checks Korg return status for success/failure *
 *      92.03.02.01  KJC  Changed fid from a pointer to an array, due   *
 *                        to technical difficulties in another program. *
 *      92.03.03.01  KJC  Report on different types of dumps.           *
 *      92.03.04.01  KJC  Changed some of the while loops to improve    *
 *                        speed... I hope.                              *
 *      92.03.24.01  KJC  Added code to change the mode to GLOBAL.      *
 *      92.03.31.01  KJC  Now the program chooses its own directory...  *
 *      92.04.10.01  KJC  Retries on Global Memory Allocation failures  *
 *                        instead of exiting.                           *
 *      92.04.13.01  KJC  Incorporate it as a function into monster.    *
 *      92.04.20.01  KJC  Added checks for M1/file memory mismatches.   *
 *      92.04.21.01  KJC  Added a computed file length check.           *
 *                                                                      *
 ************************************************************************/

int loader()
{
 unsigned char *mem,*head,*tail,*dstart,*item;  /* Dynamic mem Pointers   */
 unsigned char strm;
 unsigned int  byts,maxpgm;
 unsigned int  seqlen,cmbpgm,guess;
 long dumpsize;
 FILE *fin;

 if (fid[0] != '\0')                            /* Name on command line   */
 {
  printf("Loading Korg from %s.\n",fid);
  fflush(stdout);
 }
 else
 {
  strcpy(fid,"d:m1dump.all");                   /* Use default name.        */
  printf("Enter name of source file [%s]: ",fid);
  fflush(stdout);                               /* Ask it NOW!              */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%s",fid);                        /* Or, use a real name      */
 }

 while ((fin = fopen(fid,"rb")) == NULL)        /* Non-existent file        */
 {
  printf("File %s does not exist.  Quit? [N]: ",fid);
  fflush(stdout);                               /* Ask it NOW!              */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   if ((buf[0] & 0x5F) == 'Y')                  /* What's the answer?       */
    return 0;
   else
   {
     printf("Enter name of source file [%s]: ",fid);
     fflush(stdout);                            /* Ask it NOW!              */
     if (fgets(buf,sizeof(buf),stdin) != NULL)
      sscanf(buf,"%s",fid);                     /* Or, use a real name      */
   }
 }

 (void) fseek(fin,0L,2);                        /* Seek EOF               */
 dumpsize = ftell(fin);                         /* How big is it?         */
 if ((head = lcalloc(dumpsize,1L)) == NULL)     /* Get LOTS of memory     */
 {
   printf("The memory allocation failed... Aborting.\n");
   return 0;                                    /* Return a failure code  */
 }
 (void) fseek(fin,0L,0);                        /* Rewind file            */
 (void) fread(head,(int) dumpsize,1,fin);       /* Fill memory with it    */
 fclose(fin);                                   /* We're done w/ the file */
 printf("%ld bytes read from %s\n",dumpsize,fid);

 tail = head+dumpsize;                          /* Search all used memory */

 if ((mem = FindDump(head,tail,4)) == NULL)     /* Find start of dump     */
 {
  free(head);                                   /* Not found.  Die.       */
  return 0;
 }

 dstart = mem;                                  /* Mark true dump start   */

 switch (*(dstart+4))
 {
  case 0x48:
    printf("Loading All Sequences\n");
    seqlen = *(dstart+6) + (*(dstart+7)<<7);    /* Sequence lengths       */
    guess  = 9+sevn2ate(1522+(seqlen*4));       /* Guess file size        */
    mem    = dstart + 8;
    break;
  case 0x4C:
    printf("Loading All Programs\n");
    maxpgm = (*(dstart+5) & 2) ? 50 : 100;      /* Highest Comb or Prog # */
    guess  = 7+sevn2ate(maxpgm*143);            /* Guess file size        */
    mem    = dstart + 6;
    break;
  case 0x4D:
    printf("Loading All Combinations\n");
    maxpgm = (*(dstart+5) & 2) ? 50 : 100;      /* Highest Comb or Prog # */
    guess  = 7+sevn2ate(maxpgm*124);            /* Guess file size        */
    mem    = dstart + 6;
    break;
  case 0x51:
    printf("Loading All Global Data\n");
    guess  = 7+sevn2ate(861);                   /* Guess file size        */
    mem    = dstart + 8;
    break;
  case 0x50:
    printf("Loading Everything (Global, Prog, Comb, & Seq)\n");
    maxpgm = (*(dstart+5) & 2) ? 50 : 100;      /* Highest Comb or Prog # */
    seqlen = *(dstart+6) + (*(dstart+7)<<7);    /* Sequence lengths       */
    guess  = 9+sevn2ate(861);                   /* Guess file size (glob) */
    guess += sevn2ate(maxpgm*124);              /* Guess file size (comb) */
    guess += sevn2ate(maxpgm*143);              /* Guess file size (prog) */
    guess += sevn2ate(1522+(seqlen*4));         /* Guess file size (seq)  */
    mem    = dstart + 8;
    break;
  default:
    printf("This is not a Korg M1 dump.  Continue? [N]: ");
    fflush(stdout);                             /* Ask it NOW!            */
    if (fgets(buf,sizeof(buf),stdin) != NULL)
    if ((buf[0] & 0x5F) != 'Y')                 /* What's the answer?     */
    {
      free(head);                               /* Give head... back.     */
      exit();
    }
    break;
 }

 if ((*(dstart+4) != 0x51) && (*(dstart+4) != 0x48))
 {
  while ((memfmt<<1) != *(dstart+5))            /* Compare mem formats    */
  {
   printf("File and M1 memory mismatch.\n");
   printf("Please set Global Memory Allocation for %u/%u\n",maxpgm,maxpgm);
   printf("Strike any key when ready.\n");
   Cnecin();                                    /* Wait a sec...            */
   for(x=0; x<6; x++)
     Bconout(3,modrq[x]);                       /* Make a Mode Inquiry      */

   byts = 0;
   while (Bconstat(3) != 0)                     /* Get the entire message.  */
   {
    strm = (unsigned char) (Bconin(3) & 0x00FF);/* Get a masked MIDI byte   */
    if (strm != 0xFE)                           /* If not Active Sensing... */
     buf[byts++] = strm;                        /* ...save it & count it.   */
    if ((strm == 0xFE) && (byts != 0)) break;   /* Break on active sensing. */
   }
   memfmt = buf[6];                             /* Save memory allocation   */
  }
  printf("%u Programs/%u Combinations\n",maxpgm,maxpgm);
 }

 if ((*(dstart+4) == 0x50) || (*(dstart+4) == 0x48))
  printf("Sequencer data: %u bytes long.\n",seqlen);

 printf("Guestimated file size: %u bytes.\n",guess);

 printf("\n[more...]");
 fflush(stdout);                                /* Ask it NOW!            */
 Cnecin();                                      /* Wait a sec...          */
 x = 7;                                         /* Preslug buf index      */

 if ((*(dstart+4) == 0x50) || (*(dstart+4) == 0x51))
 {
   item = compress(&mem,861);                   /* Get the Global data    */
   free(item);
 }
 
 if ((*(dstart+4) == 0x50) || (*(dstart+4) == 0x4D))
   if ((mem = RollCall("Combinations",mem,124,maxpgm,0)) == NULL)
   {
     free(head);
     return 0;                                  /* Return failure code    */
   }

 if ((*(dstart+4) == 0x50) || (*(dstart+4) == 0x4C))
   if ((mem = RollCall("Programs",mem,143,maxpgm,0))     == NULL)
   {
     free(head);
     return 0;                                  /* Return failure code    */
   }

 if ((*(dstart+4) == 0x50) || (*(dstart+4) == 0x48))
   if ((mem = RollCall("Sequences",mem,96,10,20))        == NULL)
   {
     free(head);
     return 0;                                  /* Return failure code    */
   }

again:

 while (Bconstat(3) != 0)                       /* Anything in the queue? */
  strm = (unsigned char) Bconin(3);             /* Ignore and flush it.   */

 for(x=0; x<8; x++)
  Bconout(3,modch[x]);                          /* Switch to Global Mode  */

 while((strm = (unsigned char) (Bconin(3) & 0x00FF)) != 0xF7)
   ;                                            /* Read til EOX           */

 byts = 0;                                      /* Initialize variables   */

 for (mem=dstart; *mem != 0xF7; ++mem)          /* Search all used memory   */
   Bconout(3,*mem);                             /* Send byte to the Korg    */
 Bconout(3,*mem);                               /* Don't forget the EOX!    */

 while (byts == 0)                              /* Get a non-empty message  */
 {
  while (Bconstat(3) == 0)                      /* Wait for anything        */
    ;

  while (Bconstat(3) != 0)                      /* Get the entire message.  */
  {
   strm = (unsigned char) (Bconin(3) & 0x00FF); /* Get a masked MIDI byte   */
   if (strm != 0xFE)                            /* If not Active Sensing... */
    buf[byts++] = strm;                         /* ...save it & count it.   */
   if ((strm == 0xFE) && (byts != 0)) break;    /* Break on active sensing. */
  }
 }

 switch (buf[4])                                /* Process Korg status.     */
 {
  case 0x23:
    printf("\nData load completed successfully.\n");
    free(head);
    return 1;
  case 0x24:
    printf("\nData load error.  Check Global Memory Allocation.\n");
    printf("Retry? "); fflush(stdout);
    if (fgets(buf,sizeof(buf),stdin) != NULL)
    if ((buf[0] & 0x5F) != 'Y')                 /* What's the answer?     */
    {
      free(head);                               /* Give head... back.     */
      return 0;
    }
    goto again;
  default:
    printf("\nUnknown message.  Dump follows. (length = %d bytes)\n",byts);
    for (x = 0; x<byts; x++)  printf("%02x ",buf[x]);  printf("\n");
    free(head);                                 /* Give head... back.     */
    return 0;
 }
}

/************************************************************************
 *                                                                      *
 *      FindDump                                                        *
 *                                                                      *
 *  This function determines if and where in a block of memory, a valid *
 * dump header is found.  It sometimes also determines if the proper    *
 * type of dump header is found.                                        *
 *                                                                      *
 ************************************************************************/

unsigned char *FindDump(start,end,enough)
unsigned char *start,*end;
int enough;
{
 unsigned char *mem;
 unsigned int  hits;

 for (mem=start; mem<=end; ++mem)               /* Search all used memory */
 {
  hits = 0;                                     /* Assume no match found  */
  for (x=0; x<enough; ++x)                      /* Compare to window      */
  {
   if (*(mem+x) == rspns[x])                    /* Contents of (mem+x)    */
     ++hits;                                    /* Another successive hit */
   else
     hits = 0;                                  /* Break stride.          */
  }
  if (hits == enough) break;                    /* Entirely successful?   */
 }

 if (hits < enough)                             /* Can't find enuf bytes  */
 {
  printf("Something failed... No Dump header found.\n");
  mem = NULL;                                   /* Return a failure code  */
 }
 return mem;
}

/************************************************************************
 *                                                                      *
 *      RollCall                                                        *
 *                                                                      *
 *  This function prints the number and names of all combinations,      *
 * programs or sequences, five to a line.                               *
 *                                                                      *
 ************************************************************************/

unsigned char *RollCall(dtype,mem,dsize,count,offset)
unsigned char *dtype,*mem;
int dsize,count,offset;
{
  unsigned int  n,cmbpgm;
  unsigned char *item;

  printf("\n            %s\n\n",dtype);
  for (cmbpgm=0; cmbpgm<count; ++cmbpgm)        /* For 50 or 100 combs    */
  {
   if ((item = compress(&mem,dsize)) == NULL)
    return 0;                                   /* Failure...             */
   printf("%02d: ",cmbpgm);                     /* Comb/Prog/Seq number   */
   for (n=offset; n<(offset+10); ++n)           /* Comb/Prog/Seq name     */
   {
    if (item[n] < ' ')                          /* If non-printable...    */
      item[n] = ' ';                            /* ...make it a space     */
    fputc(item[n],stdout);
   }
   item[n] = ((cmbpgm+1) % 5) ? ' ' : '\n';     /* Print 5 on a line      */
   fputc(item[n],stdout);
   free(item);
  }
  printf("\n[more...]");
  fflush(stdout);                               /* Ask it NOW!            */
  Cnecin();                                     /* Wait for keypress      */
  return mem;                                   /* Success...             */
}

/************************************************************************
 *                                                                      *
 *      sevn2ate                Written by Kevin Cole   10-Mar-92       *
 *                                                                      *
 *  This function returns the number of 7-bit bytes necessary to store  *
 * Korg 8-bit bytes externally as MIDI data.                            *
 *                                                                      *
 ************************************************************************/

int sevn2ate(intsize)
int intsize;                                    /* Internal (Korg M1) size    */
{
 int extsize;                                   /* External (MIDI file) size  */
 extsize = (intsize / 7) * 8;                   /* Seven 8-bit to eight 7-bit */
 if ((intsize % 7) > 0)
   extsize = extsize + 1 + (intsize % 7);       /* Take care of leftovers     */
 return extsize;                                /* Return external size       */
}

/************************************************************************
 *                                                                      *
 *      compress                Written by Kevin Cole   18-Apr-92       *
 *                                                                      *
 *  This is GREAT!  It successfully decyphers portions of the rather    *
 * cryptic format of Korg M1 dumps.  Data from the Korg is 8-bits,      *
 * internally.  However, to send this as MIDI, the high order bit must  *
 * be stripped for non-command data.  This presents a problem.  Korg    *
 * chooses to get around this problem by turning seven 8-bit bytes into *
 * eight 7-bit bytes.  The first byte in the sequence contains the high *
 * order bit for each of the remaining seven bytes.                     *
 *                                                                      *
 *  Calling sequence:                                                   *
 *                                                                      *
 *              item = compress(curnt,itemsize)                         *
 *                                                                      *
 *   where      curnt   is the current input memory address.  This is   *
 *                      updated, skipping over processed memory.        *
 *              itmsiz  is the *compressed* length (ie internal length, *
 *                      not MIDI length) of the data to be compressed.  *
 *    and       item    is a pointer returned to the calling routine,   *
 *                      which points to the compressed data.            *
 *                                                                      *
 ************************************************************************/

unsigned char *compress(curnt,itemsize)
unsigned char **curnt;
unsigned int  itemsize;
{
 extern unsigned char *calloc();
 unsigned char *item;
 unsigned int  pos;

 if ((item = calloc(itemsize,1)) == NULL)       /* Get LOTS of memory     */
 {
  printf("The memory allocation failed... Aborting.\n");
  return 0;                                     /* Return a failure code  */
 }
 else
 {
  for (pos=0; pos<itemsize; ++pos)
  {
   if (x == 7)                                  /* Buffer is empty        */
   {
    for (x=0; x<7; ++x)                         /* Fit 8 bytes into 7     */
     buf[x] = (**curnt & (1<<x)) ? (*(*curnt+1+x) | 0x80) : *(*curnt+1+x);
    *curnt+=8;                                  /* Skip 8 bytes           */
    x = 0;                                      /* Goto start of buffer   */
   }
   item[pos] = buf[x++];                        /* Store assembled byte   */
  }
 }
 return item;                                   /* Tell 'em where it's at */
}
