/************************************************************************
 *                                                                      *
 *      uudecode                Copied from Internet    29-Jan-92       *
 *                                                                      *
 *   This version of uudecode was found attached to a Korg MIDI dump in *
 * a unix shell archive (.shar) file...  On the Amiga, it appears to    *
 * ALMOST work right.  It introduces ASCII NUL's into the stream where  *
 * I don't think there should be any.  This may be caused by a lack of  *
 * carriage returns in the encoded file.  Further analysis required.    *
 *                                                                      *
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define DEC(c)  (((c) - ' ') & 077)

void main()
{
  int n;
  char dest[128], a,b,c,d;

  scanf("begin %o ", &n);                       /* Read an octal number     */
  fgets(dest, 128, stdin);                      /* Get destination filename */

  if (freopen(dest, "w", stdout) == NULL)       /* Attempt to open it       */
  {                                             /* If unsuccessful...       */
    perror(dest);                               /* ... process error...     */
    exit(EXIT_FAILURE);                         /* ... and exit.            */
  }

  while ((n=getchar()) != EOF && (n=DEC(n))!=0) /* While not EOF & not NUL..*/
  {
    while (n>0)                                 /* ...While not NUL? ...    */
    {
      a = DEC(getchar());                       /* ...get 4 characters...   */
      b = DEC(getchar());
      c = DEC(getchar());
      d = DEC(getchar());
      if (n-- > 0) putchar(a << 2 | b >> 4);    /* Decide which bytes to    */
      if (n-- > 0) putchar(b << 4 | c >> 2);    /* mix and mix them.        */
      if (n-- > 0) putchar(c << 6 | d);
    }
    n=getchar();                                /* Get next positn pointer  */
  }
  exit(EXIT_SUCCESS);
}
