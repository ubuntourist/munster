/************************************************************************
 * howmuch.c                                                            *
 * Rewritten by Kevin Cole <ubuntourist@hacdc.org> 2021.04.04 (kjc)     *
 *                                                                      *
 * Originally for the Atari ST-1040, this printed out the maximum       *
 * amount of memory available for allocation. I think it does again,    *
 * some 19 years later... See:                                          *
 *                                                                      *
 *     Stack Overflow: Maximum size of malloc()                         *
 *     https://stackoverflow.com/a/13202597/447830                      *
 *                                                                      *
 * However, while the code now runs, the result is vastly larger than   *
 * the number provided by the "free" command...                         *
 *                                                                      *
 ************************************************************************/

#include <stdlib.h>
#include <stdio.h>

void main()
{
  long int low, high, midway, magnitude;
  void *block;          // block of memory

  // Allocate memory sizes in successive powers of two until failure
  //
  for (magnitude = 1; block = malloc(magnitude); magnitude <<= 1)
    free(block);

  // Using the final power of two (that failed) and the penultimate
  // (that succeeded), interpolate...
  //
  for (low = (magnitude >> 1), high = magnitude; low < high - 1;)
    {
      midway = (low + high) >> 1;  // Same as (low + high) / 2
      if (block = malloc(midway))  // If allocation is successful...
        {
          low = midway;            // ...increase the low end
          free(block);
        }
      else                         // If allocation failed...
        high = midway;             // ...decrease the high end
    }
  printf("[%ld bytes of memory free]\n", midway);
}
