/************************************************************************
 *                                                                      *
 *      vax.h                   Written by Kevin Cole   25-Mar-92       *
 *                                                                      *
 *   This is a dummy header file to allow the Atari MIDI functions to   *
 * do something halfway intelligent on the VAX.                         *
 *                                                                      *
 ************************************************************************/
#define lcalloc calloc
int Bconin(size_t port);                        /* int port */
int Bconout(size_t port,size_t outch);          /* unsigned char outch */
int Bconstat(size_t port);
