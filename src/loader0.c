/************************************************************************
 *                                                                      *
 *       loader.c               Written by Kevin Cole   30-Jan-92       *
 *                              Last revised:           02-Feb-92       *
 *                                                                      *
 *  This program is based on reader.c.  It's supposed to load a MIDI    *
 * dump back to a Korg M1...                                            *
 *                                                                      *
 ************************************************************************/

#include "osbind.h"
#include <stdio.h>

main()
{
 int strm;
 FILE *fin;
 char buf[80],fid[80];                          /* User supplied File ID */

 printf("Enter name of source file [d:m1all.dmp]: ");
 fflush(stdout);                                /* Ask it NOW! */
 strcpy(fid,"d:m1all.dmp");                     /* Use default name.    */
 if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%s",fid);                        /* Or, use a real name  */
 fin = fopen(fid,"rb");
 while ((strm = fgetc(fin)) != EOF)
 {
  Bconout(3,strm);
 }
 fclose(fin);
 exit();
}
