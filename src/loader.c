/************************************************************************
 *                                                                      *
 *       loader.c               Written by Kevin Cole   30-Jan-92       *
 *                              Last revised:           14-Apr-92       *
 *                                                                      *
 *  This program is based on reader.c.  It's supposed to load a MIDI    *
 * dump back to a Korg M1...                                            *
 *                                                                      *
 *   IMPORTANT: The Atari is sensitive to the file extension.  In order *
 * for this to run correctly from both the desktop and msh, the file    *
 * extension must be either .TPP or .TOS.  So, complie this on the      *
 * Atari with the command:                                              *
 *                                                                      *
 *              cc -o loader.tos  loader.c                              *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.02.15.01  KJC  Added GEM support for user-friendliness.      *
 *      92.02.15.01  KJC  Removed GEM support cuz it don't work.        *
 *      92.02.25.01  KJC  Supports filename on command line.            *
 *      92.02.25.02  KJC  Checks for non-existent file.                 *
 *      92.02.25.03  KJC  Checks Korg return status for success/failure *
 *      92.03.02.01  KJC  Changed fid from a pointer to an array, due   *
 *                        to technical difficulties in another program. *
 *      92.03.03.01  KJC  Report on different types of dumps.           *
 *      92.03.04.01  KJC  Changed some of the while loops to improve    *
 *                        speed... I hope.                              *
 *      92.03.24.01  KJC  Added code to change the mode to GLOBAL.      *
 *      92.03.31.01  KJC  Now the program chooses its own directory...  *
 *      92.04.14.01  KJC  Temporarily changed MIDI global channel.      *
 *                                                                      *
 ************************************************************************/

#include "osbind.h"
#include <stdio.h>
#include "misc.h"
#include "korg.h"

main(argc,argv)
int argc;  char *argv[];
{
 unsigned int strm,x,byts;
 FILE *fin;
 unsigned char buf[80],fid[80];                 /* User supplied File ID    */
 unsigned char modch[8] = {0xF0,0x42,0x3F,0x19,0x4E,0x04,0x10,0xF7};
 unsigned char rspns[7] = {0xF0,0x42,0x3F,0x19,0x50,0x00,0xF7};

 appl_init();  init_GEM();                      /* Try GEM stuff again    */
 printf("\n\n");                                /* ANSI Screen Clear      */

 printf("             Korg M1 MIDI Load  v2.0  by Kevin Cole  03-Mar-92\n\n");
 printf(" This program loads program, combination and sequence data from the ");
 printf(" file of\n your choice into the M1.\n\n");

 Dsetdrv(3);                                    /* Set drive to D:        */
 Dsetpath("\\korg\\dumps");                     /* CD to subdirectory     */
 if (argc == 2)                                 /* Name on command line   */
 {
  strcpy(fid,argv[1]);
  printf("Loading Korg from %s.\n",fid);
  fflush(stdout);
 }
 else
 {
  strcpy(fid,"d:m1dump.all");                   /* Use default name.        */
  printf("Enter name of source file [%s]: ",fid);
  fflush(stdout);                               /* Ask it NOW!              */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%s",fid);                        /* Or, use a real name      */
 }

 while ((fin = fopen(fid,"rb")) == NULL)        /* Non-existent file        */
 {
  printf("File %s does not exist.  Quit? [N]: ",fid);
  fflush(stdout);                               /* Ask it NOW!              */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   if ((buf[0] & 0x5F) == 'Y')                  /* What's the answer?       */
   {
    printf("Hit any key to exit.");
    fflush(stdout);                             /* Ask it NOW!            */
    Cnecin();                                   /* Getchar, no echo       */
    appl_exit();
   }
   else
   {
     printf("Enter name of source file [%s]: ",fid);
     fflush(stdout);                            /* Ask it NOW!              */
     if (fgets(buf,sizeof(buf),stdin) != NULL)
      sscanf(buf,"%s",fid);                     /* Or, use a real name      */
   }
 }

 for (byts=0;  byts<4; byts++)
  if((strm = fgetc(fin)) != rspns[byts])        /* Check the header.        */
  {
   printf("This is not a Korg M1 dump.  Continue? [N]: ");
   fflush(stdout);                              /* Ask it NOW!              */
   if (fgets(buf,sizeof(buf),stdin) != NULL)
   if ((buf[0] & 0x5F) != 'Y')                  /* What's the answer?       */
   {
    printf("Hit any key to exit.");
    fflush(stdout);                             /* Ask it NOW!            */
    Cnecin();                                   /* Getchar, no echo       */
    appl_exit();
   }
  }

 strm = fgetc(fin);
 switch (strm)
 {
  case 0x48:
    printf("Loading All Sequences\n");                          break;
  case 0x4C:
    printf("Loading All Programs\n");                           break;
  case 0x4D:
    printf("Loading All Combinations\n");                       break;
  case 0x51:
    printf("Loading All Global Data\n");                        break;
  case 0x50:
    printf("Loading Everything (Global, Prog, Comb, & Seq)\n"); break;
  default:
    printf("This is not a Korg M1 dump.  Continue? [N]: ");
    fflush(stdout);                             /* Ask it NOW!              */
    if (fgets(buf,sizeof(buf),stdin) != NULL)
    if ((buf[0] & 0x5F) != 'Y')                 /* What's the answer?       */
    {
     printf("Hit any key to exit.");
     fflush(stdout);                            /* Ask it NOW!            */
     Cnecin();                                  /* Getchar, no echo       */
     appl_exit();
    }
    break;
 }

again:

 fclose(fin);                                   /* "Rewind" the file        */
 fin = fopen(fid,"rb");

 while (Bconstat(3) != 0)                       /* Anything in the queue?   */
  strm = Bconin(3);                             /* Ignore and flush it.     */

 for(x=0; x<8; x++)
  Bconout(3,modch[x]);                          /* Switch to Global Mode  */

 while((strm = (unsigned char) Bconin(3) & 0x00FF) != 0xF7)     /* Skim for EOX   */
   ;

 byts = 0;                                      /* Initialize variables     */

 while ((strm = fgetc(fin)) != EOF)             /* Get a byte from the file */
 {
  Bconout(3,strm);                              /* Send byte to the Korg    */
  while (Bconstat(3) != 0)                      /* Did Korg respond?        */
  {
   strm = Bconin(3) & 0x00FF;                   /* Get a masked MIDI byte   */
   if (strm != 0xFE)                            /* If not Active Sensing... */
    buf[byts++] = strm;                         /* ...save it & count it.   */
   if ((strm == 0xFE) & (byts != 0)) break;     /* Break on active sensing. */
  }
 }

 while (Bconstat(3) == 0)                       /* Wait for anything  */
  ;

 while (Bconstat(3) != 0)                       /* Get the entire message.  */
 {
  strm = Bconin(3) & 0x00FF;                    /* Get a masked MIDI byte   */
  if (strm != 0xFE)                             /* If not Active Sensing... */
   buf[byts++] = strm;                          /* ...save it & count it.   */
  if ((strm == 0xFE) & (byts != 0)) break;      /* Break on active sensing. */
 }

 switch (buf[4])                                /* Process Korg status.     */
 {
  case 0x23:
    printf("\nData load completed successfully.\n");                  break;
  case 0x24:
    printf("\nData load error.  Check Global Memory Allocation.\n");  break;
  default:
    printf("\nUnknown message.  Dump follows. (length = %d bytes)\n",byts);
    for (x = 0; x<byts; x++)  printf("%02x ",buf[x]);  printf("\n");  break;
 }

 if (buf[4] == 0x24)                            /* Retry on mem fail      */
 {
   printf("Hit any key to retry.");
   fflush(stdout);
   Cnecin();
   goto again;
 }

 fclose(fin);
 Dsetpath("\\korg");                            /* Back from whence...    */
 printf("Hit any key to exit.");
 fflush(stdout);                                /* Ask it NOW!            */
 Cnecin();                                      /* Getchar, no echo       */
 appl_exit();
}
