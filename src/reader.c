/************************************************************************
 *                                                                      *
 *       reader.c               Written by Kevin Cole   30-Jan-92       *
 *                              Last revised:           02-Feb-92       *
 *                                                                      *
 *  This program is based on scaler.c taken from the Atari MIDI book.   *
 * It's a companion piece to writer.c.  writer wrote the scaler data to *
 * a file.  It's reader's job to see if it can be read back and fed out *
 * the MIDI port.                                                       *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.02.04.01  KJC  Read file from RAM disk to speed up I/O.      *
 *      92.02.04.02  KJC  Open file as a BINARY file to avoid problems  *
 *                        with 26 being interpreted as an ASCII ^Z.     *
 *                                                                      *
 ************************************************************************/

#include "osbind.h"
#include <stdio.h>

delay()
{
 int d1,d2;
 for (d1=0; d1<1000; d1++)
 {
  for (d2=0; d2<10; d2++)
  {
   d2=d2;
  }
 }
}

note_on(channel,note,velocity)
int channel,note,velocity;
{
 Bconout(3,(128+16+channel));  Bconout(3,note); Bconout(3,velocity);
}

note_off(o_channel,o_note,o_velocity)
int o_channel,o_note,o_velocity;
{
 Bconout(3,(128+o_channel));  Bconout(3,o_note); Bconout(3,o_velocity);
}

main()
{
 int commnd,noteval,velocity,program;
 FILE *fin;

 fin = fopen("e:scales.dat","rb");
 while ((commnd = fgetc(fin)) != EOF)
 {
  switch (commnd)
  {
   case 144:                                            /* NOTE ON */
     Bconout(3,commnd);
     noteval = fgetc(fin); velocity = fgetc(fin);
     Bconout(3,noteval);   Bconout(3,velocity);
     printf("  Note on: %d...  ",noteval);
     delay();
     break;
   case 128:                                            /* NOTE OFF */
     Bconout(3,commnd);
     noteval = fgetc(fin); velocity = fgetc(fin);
     Bconout(3,noteval);   Bconout(3,velocity);
     printf(" Note off.\n");
     break;
   case 192:                                            /* PROGRAM CHANGE */
     Bconout(3,commnd);
     program = fgetc(fin);
     Bconout(3,program);
     printf("Change to program %d.\n",program);
     break;
   default:                                             /* ERROR */
      fclose(fin);
      printf(" Error in MIDI stream. (value = %d)\n",commnd);
      exit();
  }
 }
 fclose(fin);
}
