/*    scaler.c   */

#include "osbind.h"
#include <stdio.h>

delay()
{
 int d1,d2;
 for (d1=0; d1<1000; d1++)
 {
  for (d2=0; d2<10; d2++)
  {
   d2=d2;
  }
 }
}

note_on(channel,note,velocity)
int channel,note,velocity;
{
 Bconout(3,(128+16+channel));  Bconout(3,note); Bconout(3,velocity);
}

note_off(o_channel,o_note,o_velocity)
int o_channel,o_note,o_velocity;
{
 Bconout(3,(128+o_channel));  Bconout(3,o_note); Bconout(3,o_velocity);
}

main()
{
 int program,noteval;
 for (program=16; program<32; program++)
 {
  Bconout(3,(128+64+0)); Bconout(3,program);
  for (noteval=36; noteval<96; ++noteval)
  {
   note_on(0,noteval,64);  delay();  note_off(0,noteval,64);
  }
 }
}
