#include "osbind.h"

main()
{
  int drv;
  char pathbuf[66];
  char *buf;

  buf = pathbuf;
  *buf++ = (drv = Dgetdrv())+'a';
  *buf++ = ':';
  Dgetpath(buf,drv+1);
  printf("Current path: %s\n",pathbuf);
  Dsetdrv(3);
  Dsetpath("\\korg");
  buf = pathbuf;
  *buf++ = (drv = Dgetdrv())+'a';
  *buf++ = ':';
  Dgetpath(buf,drv+1);
  printf("Current path: %s\n",pathbuf);
}
