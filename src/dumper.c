/************************************************************************
 *                                                                      *
 *      dumper.c  v2.0          Written by Kevin Cole   04-Jan-92       *
 *                              Last revised:           14-Apr-92       *
 *                                                                      *
 *  This program dumps Korg M1 MIDI data to the Atari 1040 ST.          *
 *                                                                      *
 *   IMPORTANT: The Atari is sensitive to the file extension.  In order *
 * for this to run correctly from both the desktop and msh, the file    *
 * extension must be either .TPP or .TOS.  So, complie this on the      *
 * Atari with the command:                                              *
 *                                                                      *
 *              cc -o dumper.tos  dumper.c                              *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.02.06.01  KJC  Combined filer.c and dumper.c.                *
 *      92.02.18.01  KJC  Added GEM support to keep user from shell.    *
 *      92.02.18.02  KJC  Removed GEM support cuz it don't work.        *
 *      92.02.25.01  KJC  Supports filename on command line now.        *
 *      92.02.25.02  KJC  Checks for existing file before overwriting.  *
 *      92.02.29.01  KJC  Added a check for bad non-dump data.          *
 *      92.03.02.01  KJC  Changed fid from a pointer to an array, due   *
 *                        to technical difficulties in another program. *
 *      92.03.02.02  KJC  Use a huge array, instead of the RAM disk.    *
 *      92.03.02.03  KJC  Add different types of dumps.                 *
 *      92.03.03.02  KJC  Removed 92.03.02.02.  The Atari can't handle  *
 *                        arrays that large ??? (56000 bytes).          *
 *      92.03.04.01  KJC  Changed some of the while loops to improve    *
 *                        speed... I hope.                              *
 *      92.03.08.01  KJC  Finally! I got malloc to work.  Replaced RAM  *
 *                        disk e: with allocated memory... sort of.     *
 *      92.03.08.02  KJC  Implemented a sliding window search for the   *
 *                        start of the dump.  Together with the prior   *
 *                        patch, it should get a complete dump EVERY    *
 *                        time.                                         *
 *      92.03.20.01  KJC  The Atari doesn't like memset/setmem.  So     *
 *                        memory must be initialized when allocated.    *
 *                        Switched from malloc to calloc.               *
 *      92.03.24.01  KJC  Voodoo code.  60000 is less than 64K, but     *
 *                        calloc screwed everything up.  lcalloc used.  *
 *      92.03.24.01  KJC  Added a MODE CHANGE GLOBAL command.  The dump *
 *                        seems to work better if the mode is global.   *
 *      92.03.31.01  KJC  Now the program chooses its own directory...  *
 *      92.04.14.01  KJC  Temporarily changed MIDI global channel.      *
 *                                                                      *
 ************************************************************************/

#include "osbind.h"
#include <stdio.h>
#include "misc.h"
#include "korg.h"

main(argc,argv)
int argc;  char *argv[];
{
 FILE *fin, *fout;                              /* In and Out files       */
 extern unsigned char *lcalloc();
 unsigned char *mem,*head,*tail;                /* Pointers to characters */
 unsigned int dmptyp,x,byts,strm,success;       /* Miscellaneous stuff    */
 unsigned char typ[5],buf[80],fid[80];          /* User supplied File ID  */
 unsigned char modch[8] = {0xF0,0x42,0x3F,0x19,0x4E,0x04,0x10,0xF7};
 unsigned char reqst[7] = {0xF0,0x42,0x3F,0x19,0x0F,0x00,0xF7};
 unsigned char rspns[7] = {0xF0,0x42,0x3F,0x19,0x50,0x00,0xF7};

 appl_init(); init_GEM();                       /* Try GEM stuff again    */
 printf("\n\n");                                /* ANSI Screen Clear      */

 printf("             Korg M1 MIDI Dump  v2.0  by Kevin Cole  08-Mar-92\n\n");
 printf(" This is an experimental version of the dumper program.  This ");
 printf("version tries\n to use a menu and does away with the RAM disk.\n");
 printf(" Make sure that your destination disk has at least 56000 bytes ");
 printf("available for\n a full dump.  Less for the others.\n\n");

 dmptyp = 5;                                    /* Default to ALL */

again:
 printf(" Choose one of the following:\n\n");
 printf("    1 = Dump All Sequences       ( 2000 to 38000 bytes)\n");
 printf("    2 = Dump All Programs        ( 8179 or 16350 bytes)\n");
 printf("    3 = Dump All Combinations    ( 7093 or 14179 bytes)\n");
 printf("    4 = Dump All Global Data     (  991 bytes)\n");
 printf("    5 = Dump Everything          (18000 to 55000 bytes)\n\n");
 printf("Dump type (1-5) [%d]: ",dmptyp);
 fflush(stdout);                                /* Ask it NOW! */
 if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%d",&dmptyp);                    /* <-- Remember '&' trick  */

 switch (dmptyp)
 {
  case 1:
    reqst[4] = 0x18;  rspns[4] = 0x48;  strcpy(typ,".seq");  break;
  case 2:
    reqst[4] = 0x1C;  rspns[4] = 0x4C;  strcpy(typ,".pgm");  break;
  case 3:
    reqst[4] = 0x1D;  rspns[4] = 0x4D;  strcpy(typ,".cmb");  break;
  case 4:
    reqst[4] = 0x0E;  rspns[4] = 0x51;  strcpy(typ,".gbl");  break;
  case 5:
    reqst[4] = 0x0F;  rspns[4] = 0x50;  strcpy(typ,".all");  break;
  default:
    printf("That wasn't an option.  Try again.\n\n");
    goto again;
 }

 printf("\n             Hit any key to begin dump.");
 fflush(stdout);                                /* Ask it NOW!            */
 Cnecin();                                      /* Getchar, no echo       */
 printf("\n\nPlease wait...");  fflush(stdout);

 x = 0; byts = 0;                               /* Initialize variables   */
 head = NULL;                                   /* "Break" it first       */
 if ((head = lcalloc(60000L,1L)) == NULL)       /* Get LOTS of memory     */
 {
   printf("The memory allocation failed... Aborting.\n");
   printf("Hit any key to exit.");
   fflush(stdout);                              /* Ask it NOW!            */
   Cnecin();                                    /* Getchar, no echo       */
   appl_exit();
 }

 while (Bconstat(3) != 0)                       /* Anything in the queue? */
  strm = Bconin(3);                             /* Ignore and flush it.   */

 for(x=0; x<8; x++)
  Bconout(3,modch[x]);                          /* Switch to Global Mode  */

 tail = head;                                   /* Point to the start     */
 while((*tail++ = (unsigned char) Bconin(3) & 0x00FF) != 0xF7)  
   ;                                            /* Skim for EOX           */

 for(x=0; x<7; x++)
  Bconout(3,reqst[x]);                          /* Request Dump           */

 tail = head;                                   /* Point to the start     */
 while((*tail++ = (unsigned char) Bconin(3) & 0x00FF) != 0xF7)
   ;                                            /* Skim for EOX           */

 byts = (tail - head) + 1;                      /* How long has it been?  */
 printf("  So far, so good.\n");
 printf("Korg dump (programs, combinations and sequences) now in memory.\n");
 printf("%u bytes temporarily used.\n",byts);   /* 33 days, sir.  */

 for (mem=head; mem<=tail; ++mem)               /* Search all used memory */
 {
  success = 0;                                  /* Assume no match found  */
  for (x=0; x<5; ++x)                           /* Compare 6-byte window  */
  {
   if (*(mem+x) != rspns[x])                    /* Contents of (mem+x)    */
     success = 0;                               /* Break stride.          */
   else
     ++success;                                 /* Another successive hit */
  }
  if (success == 5) break;                      /* Entirely successful?   */
 }

 if (success != 5)                              /* Couldn't find 6 bytes  */
 {
  printf("Something failed... No Dump header found.\n");
  free(head);
  printf("Hit any key to exit.");
  fflush(stdout);                               /* Ask it NOW!            */
  Cnecin();                                     /* Getchar, no echo       */
  appl_exit();
 }

 Dsetdrv(3);                                    /* Set drive to D:        */
 Dsetpath("\\korg\\dumps");                     /* CD to subdirectory     */

 if (argc == 2)                                 /* Name on command line   */
 {
  strcpy(fid,argv[1]);
  printf("Dump will be saved in %s.\n",fid);
  fflush(stdout);
 }
 else
 {
  strcpy(fid,"d:m1dump");  strcat(fid,typ);     /* Use default name.      */
  printf("Enter name of destination file ending with %s [%s]: ",typ,fid);
  fflush(stdout);                               /* Ask it NOW!            */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%s",fid);                        /* Or, use a real name    */
 }

 while ((fin = fopen(fid,"rb")) != NULL)        /* Prevent overwriting    */
 {
  fclose(fin);
  printf("File %s already exists.  Overwrite it? [N]: ",fid);
  fflush(stdout);                               /* Ask it NOW!            */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   if ((buf[0] & 0x5F)== 'Y')                   /* What's the answer?     */
    break;
   else
   {
     printf("Enter name of destination file [%s]: ",fid);
     fflush(stdout);                            /* Ask it NOW!            */
     if (fgets(buf,sizeof(buf),stdin) != NULL)
      sscanf(buf,"%s",fid);                     /* Or, use a real name    */
   }
 }
 fout = fopen(fid,"wb");                        /* Where to dump to       */

 byts = 0;
 while(mem<=tail)                               /* Starting with header   */
 {
  fputc(*mem++,fout);                           /* Write out the file     */
  ++byts;                                       /* and count bytes.       */
 }

 free(head);                                    /* Give head... back.     */
 fclose(fout);

 printf("Dump saved to %s. %u bytes\n",fid,byts);       /* Print totals */

 Dsetpath("\\korg");                            /* CD back to start */
 printf("Hit any key to exit.");
 fflush(stdout);                                /* Ask it NOW!            */
 Cnecin();                                      /* Getchar, no echo       */
 appl_exit();
}
