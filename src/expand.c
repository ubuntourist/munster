/************************************************************************
 * expand.c                                                             *
 * Rewritten by Kevin Cole <ubuntourist@hacdc.org> 2021.04.04 (kjc)     *
 *                                                                      *
 * Originally for the Atari ST-1040, this code convers 8-bit bytes to   *
 * 7-bit bytes and computes the new size needed to accommodate the      *
 * "refugee" bits.                                                      *
 *                                                                      *
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>

void main()
{
  char buf[80];
  int truesize,fakesize;

  printf("Enter number to be expanded: ");
  fflush(stdout);                              /* Ask it NOW! */
  if (fgets(buf,sizeof(buf),stdin) != NULL)    /* Get a string */
    sscanf(buf,"%d",&truesize);                /* If it's not empty, parse */
  fakesize = (truesize / 7) * 8;               /* Seven 8-bit to eight 7-bit */
  if ((truesize % 7) > 0)
    fakesize = fakesize + 1 + (truesize % 7);  /* Take care of leftovers */
  printf("%d --> %d\n",truesize,fakesize);     /* Print it to check result */
  exit(EXIT_SUCCESS);
}
