/************************************************************************
 *                                                                      *
 *       writer.c               Written by Kevin Cole   30-Jan-92       *
 *                              Last Revised:           04-Feb-92       *
 *                                                                      *
 *  This program is based on scaler.c taken from the Atari MIDI book.   *
 * But, this time we send all the goodies to a binary file.  Later, we  *
 * read the file and pump it out the MIDI line.                         *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.02.04.01  KJC  Write file to RAM disk to speed up I/O.       *
 *      92.02.04.02  KJC  Open file as a BINARY file to avoid problems  *
 *                        with 26 being interpreted as an ASCII ^Z.     *
 *                                                                      *
 ************************************************************************/

#include "osbind.h"
#include <stdio.h>

delay()
{
 int d1,d2;
 for (d1=0; d1<1000; d1++)
 {
  for (d2=0; d2<10; d2++)
  {
   d2=d2;
  }
 }
}

note_on(channel,note,velocity)
int channel,note,velocity;
{
 Bconout(3,(128+16+channel));  Bconout(3,note); Bconout(3,velocity);
}

note_off(o_channel,o_note,o_velocity)
int o_channel,o_note,o_velocity;
{
 Bconout(3,(128+o_channel));  Bconout(3,o_note); Bconout(3,o_velocity);
}

main()
{
 int program,noteval;
 FILE *fout;

 fout = fopen("e:scales.dat","wb");
 for (program=16; program<32; program++)
 {
  fputc((128+64+0),fout); fputc(program,fout);               /* PROG CHANGE */
  for (noteval=36; noteval<96; ++noteval)
  {
   fputc((128+16+0),fout); fputc(noteval,fout); fputc(64,fout); /* NOTE ON  */
   fputc((128+0),fout);    fputc(noteval,fout); fputc(64,fout); /* NOTE OFF */
  }
 }
 fclose(fout);
}
