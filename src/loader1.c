/************************************************************************
 *                                                                      *
 *       loader.c               Written by Kevin Cole   30-Jan-92       *
 *                              Last revised:           02-Mar-92       *
 *                                                                      *
 *  This program is based on reader.c.  It's supposed to load a MIDI    *
 * dump back to a Korg M1...                                            *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.02.15.01  KJC  Added GEM support for user-friendliness.      *
 *      92.02.15.01  KJC  Removed GEM support cuz it don't work.        *
 *      92.02.25.01  KJC  Supports filename on command line.            *
 *      92.02.25.02  KJC  Checks for non-existent file.                 *
 *      92.02.25.03  KJC  Checks Korg return status for success/failure *
 *      92.03.02.01  KJC  Changed fid from a pointer to an array, due   *
 *                        to technical difficulties in another program. *
 *                                                                      *
 ************************************************************************/

#include "osbind.h"
#include <stdio.h>
#include "korg.h"

/* GEM Iniitialization stuff.  Pre-slug the intin array, get a "handle" *
 * and open a virtual workstation.... whatever that means.              */

/*
int intin[256],intout[256],ptsin[256],ptsout[256],contrl[12],handle,dummy;
init_GEM()
{
 int i;
 for (i=0; i<10; i++) intin[i] = 1; intin[10] = 2;
 handle = graf_handle(&dummy,&dummy,&dummy,&dummy);
 v_opnvwk(intin,&handle,intout);
}
*/

unsigned int gflag;                             /* global flag */

from_MIDI()
{
 int ifm;  ifm = Bconin(3) & 0x00FF;  return(ifm);
}

MIDI_status()
{
 int ims;  ims = Bconin(3); if(ims != 0) ims = 1; return(ims);
}

check_MIDI()
{
 int flag,ret;
 flag = MIDI_status();
 switch(flag)
 {
  case 0:
    gflag = 1;
    break;
  default:
    ret = from_MIDI();
    break;
 }
 return(ret);
}

flush_MIDI()
{
 gflag = 0; while (gflag == 0) check_MIDI();
}

main(argc,argv)
int argc;  char *argv[];
{
 unsigned int strm,x,byts;
 FILE *fin;
 unsigned char buf[80],fid[80];                 /* User supplied File ID    */
 unsigned char rspns[7] = {0xF0,0x42,0x30,0x19,0x50,0x00,0xF7};

/* appl_init();  init_GEM();  v_clrwk(handle);  / Init GEM and clear screen */

 printf("\n");
 printf("             Korg M1 MIDI Load  v1.0  by Kevin Cole  25-Feb-92\n\n");
 printf(" This program loads program, combination and sequence data from the ");
 printf(" file of\n your choice into the M1.\n\n");

 if (argc == 2)                                 /* Name on command line     */
 {
  strcpy(fid,argv[1]);
  printf("Loading Korg from %s.\n",fid);
  fflush(stdout);
 }
 else
 {
  strcpy(fid,"d:m1all.dmp");                    /* Use default name.        */
  printf("Enter name of source file [%s]: ",fid);
  fflush(stdout);                               /* Ask it NOW!              */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%s",fid);                        /* Or, use a real name      */
 }

 while ((fin = fopen(fid,"rb")) == NULL)        /* Non-existent file        */
 {
  printf("File %s does not exist.  Quit? [N]: ",fid);
  fflush(stdout);                               /* Ask it NOW!              */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   if ((buf[0] & 0x5F) == 'Y')                  /* What's the answer?       */
    exit();
   else
   {
     printf("Enter name of source file [%s]: ",fid);
     fflush(stdout);                            /* Ask it NOW!              */
     if (fgets(buf,sizeof(buf),stdin) != NULL)
      sscanf(buf,"%s",fid);                     /* Or, use a real name      */
   }
 }

====================
 for (byts =0;  byts < 6; byts++)
  if((strm = fgetc(fin)) != rspns[byts])        /* Check the header.        */
  {
   printf("This is not a Korg M1 dump.  Continue? [N]: ");
   fflush(stdout);                              /* Ask it NOW!              */
   if (fgets(buf,sizeof(buf),stdin) != NULL)
   if ((buf[0] & 0x5F) != 'Y')                  /* What's the answer?       */
    exit();
  }

 fclose(fin);
 fin = fopen(fid,"rb");
========================   

 while (Bconstat(3))                            /* Anything in the queue?   */
  strm = Bconin(3);                             /* Ignore and flush it.     */

 byts = 0;                                      /* Initialize variables     */

 while ((strm = fgetc(fin)) != EOF)             /* Get a byte from the file */
 {
  Bconout(3,strm);                              /* Send byte to the Korg    */
  while (Bconstat(3))                           /* Did Korg respond?        */
  {
   strm = Bconin(3) & 0x00FF;                   /* Get a masked MIDI byte   */
   if (strm != 0xFE)                            /* If not Active Sensing... */
   {
    buf[byts] = strm; ++byts;                   /* ...save it & count it.   */
   }
   if ((strm == 0xFE) & (byts != 0)) break;     /* Break on active sensing. */
  }
 }

 while (x == x)                                 /* Wait for anything  */
 {
  if (Bconstat(3)) break;                       /* Something happened */
 }

 while (Bconstat(3))                            /* Get the entire message.  */
 {
  strm = Bconin(3) & 0x00FF;                    /* Get a masked MIDI byte   */
  if (strm != 0xFE)                             /* If not Active Sensing... */
  {
   buf[byts] = strm; ++byts;                    /* ...save it & count it.   */
  }
  if ((strm == 0xFE) & (byts != 0)) break;      /* Break on active sensing. */
 }

 switch (buf[4])                                /* Process Korg status.     */
 {
  case 0x23:
    printf("\nData load completed successfully.\n");                  break;
  case 0x24:
    printf("\nData load error.  Check Global Memory Allocation.\n");  break;
  default:
    printf("\nUnknown message.  Dump follows. (length = %d bytes)\n",byts);
    for (x = 0; x<byts; x++)  printf("%02x ",buf[x]);  printf("\n");  break;
 }
 fclose(fin);
 exit();                                        /* Exit GEM nicely */
}
=======
 byts = 0;                                      /* Point to first element */
 while((strm = fgetc(fin)) != EOF)              /* REALLY save the file   */
 {
  if(byts < 6)                                  /* Examine first 6 bytes  */
  {
   if(strm == rspns[byts])                      /* Is it the real header? */
   {
    fputc(strm,fout);  byts++;                  /* If so, save it.        */
   }
  }
  else
  {
   fputc(strm,fout);  byts++;                   /* Past header. Save it.  */
   if (strm == rspns[6]) break;                 /* Stop after writing EOX */
  }
 }
