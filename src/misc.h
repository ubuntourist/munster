/************************************************************************
 *                                                                      *
 *      misc.h                  Written by Kevin Cole   10-Feb-92       *
 *                                                                      *
 *   Well, not exactly written by me.  Full credit for this goes to the *
 * authors of "Atari ST Introduction to MIDI Programming".  This is the *
 * GEM stuff that I don't understand, so I'm not really including it in *
 * any of my programs yet.  Eventually, I'd like to use it to eliminate *
 * the need to go into the shell (MSH).  Add a few nice menus, etc.     *
 *   It actually does contain a few MIDI routines as well, and I might  *
 * use them if I felt they worked better than the in-line code I have   *
 * now...                                                               *
 *                                                                      *
 ************************************************************************/

/* GEM Iniitialization stuff.  Pre-slug the intin array, get a "handle" *
 * and open a virtual workstation.... whatever that means.              */

int intin[256],intout[256],ptsin[256],ptsout[256],contrl[12],handle,dummy;

void init_GEM()
{
 int i;
 for (i=0; i<10; i++) intin[i] = 1; intin[10] = 2;
 handle = graf_handle(&dummy,&dummy,&dummy,&dummy);
 v_opnvwk(intin,&handle,intout);
}

unsigned int gflag;                             /* global flag */

int from_MIDI()
{
 int ifm;  ifm = Bconin(3) & 0x00FF;  return(ifm);
}

int MIDI_status()
{
 int ims;  ims = Bconin(3); if(ims != 0) ims = 1; return(ims);
}

int check_MIDI()
{
 int flag,ret;
 flag = MIDI_status();
 switch(flag)
 {
  case 0:
    gflag = 1;
    break;
  default:
    ret = from_MIDI();
    break;
 }
 return(ret);
}

void flush_MIDI()
{
 gflag = 0; while (gflag == 0) check_MIDI();
}
