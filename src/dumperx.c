/************************************************************************
 *                                                                      *
 *      dumper.c  v1.0          Written by Kevin Cole   04-Jan-92       *
 *                              Last revised:           04-Mar-92       *
 *                                                                      *
 *  This program is based on dumper.c.  It's supposed to stuff a MIDI   *
 * dump into an Atari file...  The original tried to initiate the dump  *
 * and had problems.  This version waits for dump to be initiated from  *
 * the Korg M1 keyboard.                                                *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.02.06.01  KJC  Combined filer.c and dumper.c.                *
 *      92.02.18.01  KJC  Added GEM support to keep user from shell.    *
 *      92.02.18.02  KJC  Removed GEM support cuz it don't work.        *
 *      92.02.25.01  KJC  Supports filename on command line now.        *
 *      92.02.25.02  KJC  Checks for existing file before overwriting.  *
 *      92.02.29.01  KJC  Added a check for bad non-dump data.          *
 *      92.03.02.01  KJC  Changed fid from a pointer to an array, due   *
 *                        to technical difficulties in another program. *
 *      92.03.02.02  KJC  Use a huge array, instead of the RAM disk.    *
 *      92.03.02.03  KJC  Add different types of dumps.                 *
 *      92.03.03.02  KJC  Removed 92.03.02.02.  The Atari can't handle  *
 *                        arrays that large ??? (56000 bytes).          *
 *      92.03.04.01  KJC  Changed some of the while loops to improve    *
 *                        speed... I hope.                              *
 *                                                                      *
 ************************************************************************/

#include "osbind.h"
#include <stdio.h>
#include "korg.h"

/* GEM Iniitialization stuff.  Pre-slug the intin array, get a "handle" *
 * and open a virtual workstation.... whatever the hell that means.     */

/* Commenting out the unused stuff for now...   */
/* --------------------------------------------------------------------- */
/*
int intin[256],intout[256],ptsin[256],ptsout[256],contrl[12],handle,dummy;
init_GEM()
{
 int i;
 for (i=0; i<10; i++) intin[i] = 1; intin[10] = 2;
 handle = graf_handle(&dummy,&dummy,&dummy,&dummy);
 v_opnvwk(intin,&handle,intout);
}

int gflag;                                      !* global flag *!

from_MIDI()
{
 int ifm;  ifm = Bconin(3) & 0x00FF;  return(ifm);
}

MIDI_status()
{
 int ims;  ims = Bconstat(3); if(ims != 0) ims = 1; return(ims);
}

check_MIDI()
{
 int flag,ret;
 flag = MIDI_status();
 switch(flag)
 {
  case 0:
    gflag = 1;
    break;
  default:
    ret = from_MIDI();
    break;
 }
 return(ret);
}

flush_MIDI()
{
 gflag = 0; while (gflag = 0) check_MIDI();
}
*/

/* Everthing above this line has been commented out...  */
/* --------------------------------------------------------------------------*/

main(argc,argv)
int argc;  char *argv[];
{
 FILE *fout;                                    /* Where to write to      */
 FILE *fin;                                     /* Where to read from     */
 unsigned int dmptyp,x,byts,strm;
 unsigned char typ[5],buf[80],fid[80];          /* User supplied File ID  */
 unsigned char reqst[7] = {0xF0,0x42,0x30,0x19,0x0F,0x00,0xF7};
 unsigned char rspns[7] = {0xF0,0x42,0x30,0x19,0x50,0x00,0xF7};

/* appl_init();  init_GEM();  v_clrwk(handle);  / Init GEM and clear screen */

 printf("             Korg M1 MIDI Dump  v2.0  by Kevin Cole  02-Mar-92\n\n");
 printf(" This is an experimental version of the dumper program.  This ");
 printf("version tries\n to use a menu and does away with the RAM disk.\n");
 printf(" Make sure that your destination disk has at least 56000 bytes ");
 printf("available for\n a full dump.  Less for the others.\n\n");

 dmptyp = 5;                                    /* Default to ALL */

again:
 printf(" Choose one of the following:\n\n");
 printf("    1 = Dump All Sequences       ( 2000 to 38000 bytes)\n");
 printf("    2 = Dump All Programs        ( 8179 or 16350 bytes)\n");
 printf("    3 = Dump All Combinations    ( 7093 or 14179 bytes)\n");
 printf("    4 = Dump All Global Data     (  991 bytes)\n");
 printf("    5 = Dump Everything          (18000 to 55000 bytes)\n\n");
 printf("Dump type (1-5) [%d]: ",dmptyp);
 fflush(stdout);                                /* Ask it NOW! */
 if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%d",&dmptyp);                    /* <-- Remember '&' trick  */

 switch (dmptyp)
 {
  case 1:
    reqst[4] = 0x18;  rspns[4] = 0x48;  strcpy(typ,".seq");  break;
  case 2:
    reqst[4] = 0x1C;  rspns[4] = 0x4C;  strcpy(typ,".pgm");  break;
  case 3:
    reqst[4] = 0x1D;  rspns[4] = 0x4D;  strcpy(typ,".cmb");  break;
  case 4:
    reqst[4] = 0x0E;  rspns[4] = 0x51;  strcpy(typ,".gbl");  break;
  case 5:
    reqst[4] = 0x0F;  rspns[4] = 0x50;  strcpy(typ,".all");  break;
  default:
    printf("That wasn't an option.  Try again.\n\n");
    goto again;
 }
 printf("\n             Type <RETURN> to begin dump.");
 fflush(stdout);                                /* Ask it NOW!            */
 fgets(buf,sizeof(buf),stdin);
 printf("\n\nPlease wait...");  fflush(stdout);

 x = 0; byts = 0;                               /* Initialize variables   */
 fout = fopen("e:m1dump.tmp","wb");             /* Where to dump to       */
 while (Bconstat(3) != 0)                       /* Anything in the queue? */
  strm = Bconin(3);                             /* Ignore and flush it.   */

 while (Bcostat(3) == 0)                        /* Til MIDI out ready...  */
   ;                                            /* ...spin wheels         */
 for(x=0; x<7; x++)
  Bconout(3,reqst[x]);                          /* Request Dump           */

 while((strm = Bconin(3) & 0x00FF) != 0xF0)     /* Skim MIDI for SYS EX   */
   ;
 fputc(strm,fout); ++byts;                      /* Save it and count it   */

 while((strm = Bconin(3) & 0x00FF) != 0xFE)     /* Til Active Sensing do  */
 {
  fputc(strm,fout); ++byts;                     /* Save it and count it   */
 }

 fclose(fout);

 printf("  So far, so good.\n");
 printf("Korg dump (programs, combinations and sequences) now in memory.\n");
 printf("Total size: %u\n",byts);

 if (argc == 2)                                 /* Name on command line   */
 {
  strcpy(fid,argv[1]);
  printf("Dump will be saved in %s.\n",fid);
  fflush(stdout);
 }
 else
 {
  strcpy(fid,"d:m1dump");  strcat(fid,typ);     /* Use default name.      */
  printf("Enter name of destination file ending with %s [%s]: ",typ,fid);
  fflush(stdout);                               /* Ask it NOW!            */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
  {
   sscanf(buf,"%s",fid);                        /* Or, use a real name    */
  }
 }

 while ((fin = fopen(fid,"rb")) != NULL)        /* Prevent overwriting    */
 {
  fclose(fin);
  printf("File %s already exists.  Overwrite it? [N]: ",fid);
  fflush(stdout);                               /* Ask it NOW!            */
  if (fgets(buf,sizeof(buf),stdin) != NULL)
   if ((buf[0] & 0x5F)== 'Y')                   /* What's the answer?     */
    break;
   else
   {
     printf("Enter name of destination file [%s]: ",fid);
     fflush(stdout);                            /* Ask it NOW!            */
     if (fgets(buf,sizeof(buf),stdin) != NULL)
      sscanf(buf,"%s",fid);                     /* Or, use a real name    */
   }
 }

 fin  = fopen("e:m1dump.tmp","rb");             /* Where to read from     */
 fout = fopen(fid,"wb");                        /* Where to dump to       */
 byts = 0;

 while((strm = fgetc(fin)) != EOF)              /* Til EOF do...          */
 {
  if(byts < 6)                                  /* Examine first 6 bytes  */
  {
   if(strm == rspns[byts])                      /* Is it the real header? */
   {
    fputc(strm,fout);  byts++;                  /* If so, save it.        */
   }
  }
  else
  {
   fputc(strm,fout);  byts++;                   /* Past header. Save it.  */
   if (strm == rspns[6]) break;                 /* Stop after writing EOX */
  }
 }

 fclose(fout);
 fclose(fin);
 Fdelete("e:m1dump.tmp");                       /* Get rid of tmp file  */

 printf("Dump saved to %s. %u bytes\n",fid,byts);       /* Print totals */
/* appl_exit();                                 / Exit GEM nicely */
 exit();
}
