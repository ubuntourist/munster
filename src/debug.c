/************************************************************************
 *                                                                      *
 *       debug.c                Written by Kevin Cole   04-Feb-92       *
 *                                                                      *
 *   This program is based on reader.c.  It was designed to diagnose    *
 * a problem reading scaler.dat.  Unfortunately, it never found the     *
 * problem (which turned out to be a misinterpretation of 26 as ^Z,     *
 * EOF (end of file).  The solution was to read and write binary files. *
 *   It does, however, display a growing knowledge of C in the fact     *
 * that the "doit" routine correctly passes arguments back and forth.   *
 *                                                                      *
 ************************************************************************/

#include "osbind.h"
#include <stdio.h>

doit(dev)
FILE *dev;
{
 int c,erx;
 c = fgetc(dev);
 if ((erx = ferror(dev)) != 0)
 {
  printf (" An error has occurred.  Error #: %d\n",erx);
  fclose(dev);
  exit();
 }
 return(c);
}

main()
{
 int commnd,noteval,velocity,program,n;
 FILE *fin;

 fin = fopen("e:scales.dat","rb");  n = 0;
 while ((commnd = doit(fin)) != EOF)
 {
  ++n;
  switch (commnd)
  {
   case 144:
     noteval = doit(fin); ++n; velocity = doit(fin); ++n;
     printf("   (%d) %d %d %d ... ",n,commnd,noteval,velocity);
     break;
   case 128:
     noteval = doit(fin); ++n; velocity = doit(fin); ++n;
     printf(" (%d) %d %d %d\n",n,commnd,noteval,velocity);
     break;
   case 192:
     program = doit(fin); ++n;
     printf("(%d) %d %d\n",n,commnd,program);
     break;
   default:
      fclose(fin);
      printf(" End of file... we hope. (value = %d)\n",commnd);
      exit();
  }
 }
 fclose(fin);
}
