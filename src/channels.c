/************************************************************************
 *                                                                      *
 *      channels                Written by Kevin Cole   23-Apr-92       *
 *                                                                      *
 *  This program is based on loader & analyze.  This is a one-shot      *
 * piece of code to change the global MIDI channel to 16 on all of the  *
 * old dump files.                                                      *
 *                                                                      *
 *   IMPORTANT: The Atari is sensitive to the file extension.  In order *
 * for this to run correctly from both the desktop and msh, the file    *
 * extension must be either .TPP or .TOS.  So, complie this on the      *
 * Atari with the command:                                              *
 *                                                                      *
 *              cc -o channels.tos  channels.c                          *
 *                                                                      *
 ************************************************************************/

#include <stdio.h>
#include "osbind.h"
#include "korg.h"

main()
{
 extern unsigned char *lcalloc();
 unsigned char *head;                           /* Pointers to alloc mem  */
 unsigned char fid[80],buf[80];
 long dumpsize;
 FILE *drvr;
 FILE *fio;

 drvr = fopen("files.lst","r");                 /* The list of files      */

 while (fgets(buf,sizeof(buf),drvr) != NULL)    /* Get while getting good */
 {
  sscanf(buf,"%s",fid);                         /* Make it a valid fid    */
  printf("\n%s...",fid);                        /* Status report          */
  fio = fopen(fid,"rb");
  printf("opened...");                          /* Status report          */
  (void) fseek(fio,0L,2);                       /* Seek EOF               */
  dumpsize = ftell(fio);                        /* How big is it?         */
  head = NULL;                                  /* "Break" it first       */
  if ((head = lcalloc(dumpsize,1L)) == NULL)    /* Get LOTS of memory     */
  {
    printf("The memory allocation failed... Aborting.\n");
    exit();                                     /* Return a failure code  */
  }
  (void) fseek(fio,0L,0);                       /* Rewind file            */
  (void) fread(head,(int) dumpsize,1,fio);      /* Fill memory with it    */
  printf("read...");                            /* Status report          */
  if (*(head+2) == 0x30)                        /* Old channel: MIDI 1    */
  {
   *(head+2) = 0x3F;                            /* Change to MIDI 16      */
   fclose(fio);                                 /* Close it, and...       */
   fio = fopen(fid,"wb");                       /* Open for writting      */
   (void) fwrite(head,(int) dumpsize,1,fio);    /* Fill file with it      */
   printf("written...");                        /* Status report          */
  }
  fclose(fio);                                  /* We're done w/ the file */
  printf("closed...");                          /* Status report          */
  free(head);                                   /* Give head... back.     */
 }
 printf("\n");
 fclose(drvr);
 exit();
}
