/************************************************************************
 *                                                                      *
 *       dumper.c               Written by Kevin Cole   04-Jan-92       *
 *                              Last revised:           02-Mar-92       *
 *                                                                      *
 *  This program is based on dumper.c.  It's supposed to stuff a MIDI   *
 * dump into an Atari file...  The original tried to initiate the dump  *
 * and had problems.  This version waits for dump to be initiated from  *
 * the Korg M1 keyboard.                                                *
 *                                                                      *
 *      Revision History                                                *
 *      ----------------                                                *
 *      92.02.06.01  KJC  Combined filer.c and dumper.c.                *
 *      92.02.17.01  KJC  Added GEM support so users won't need shell   *
 *      92.02.18.01  KJC  Removed shell support...  Didn't work right   *
 *      92.02.18.02  KJC  Messed with malloc as opposed to RAM:         *
 *      92.03.02.01  KJC  Changed fid from a pointer to an array, due   *
 *                        to technical difficulties in another program. *
 *                                                                      *
 ************************************************************************/

#include "osbind.h"
#include <stdio.h>
#include "korg.h"

/* GEM Iniitialization stuff.  Pre-slug the intin array, get a "handle" *
 * and open a virtual workstation.... whatever that means.              */

/*
int intin[256],intout[256],ptsin[256],ptsout[256],contrl[12],handle,dummy;
init_GEM()
{
 int i;
 for (i=0; i<10; i++) intin[i] = 1; intin[10] = 2;
 handle = graf_handle(&dummy,&dummy,&dummy,&dummy);
 v_opnvwk(intin,&handle,intout);
}
*/

int gflag;                                      /* global flag */

from_MIDI()
{
 int ifm;  ifm = Bconin(3) & 0x00FF;  return(ifm);
}

MIDI_status()
{
 int ims;  ims = Bconin(3); if(ims != 0) ims = 1; return(ims);
}

check_MIDI()
{
 int flag,ret;
 flag = MIDI_status();
 switch(flag)
 {
  case 0:
    gflag = 1;
    break;
  default:
    ret = from_MIDI();
    break;
 }
 return(ret);
}

flush_MIDI()
{
 gflag = 0; while (gflag = 0) check_MIDI();
}

main()
{
 register char *p, *head;                       /* Dunno what I'm doing */
 extern unsigned char *malloc();                /* See malloc in Mark W */
 FILE *fout;                                    /* Where to write to  */
 char buf[80],fid[80];                          /* User supplied File ID */
 unsigned int x,byts,eoxs,strm;
 unsigned char reqst[7] = {0xF0,0x42,0x30,0x19,0x0F,0x00,0xF7};
 long int maxmem = 56320;

/* appl_init();  init_GEM();  v_clrwk(handle);  / Init GEM and clear screen */

 printf("             Korg M1 MIDI Dump  v0.3  by Kevin Cole  07-Feb-92\n\n");
 printf(" This program saves program, combination and sequence data in the ");
 printf(" file of\n your choice.  Make sure that your destination disk has ");
 printf(" at least 55000 bytes\n available.\n\n");
 printf("             Type any character to begin dump.");
 fflush(stdout);                                /* Ask it NOW! */
 x = getc(stdin);                               /* Wait for keystroke */
 printf("\n\nPlease wait...");  fflush(stdout);

 x = 0; byts = 0; eoxs = 0;                     /* Initialize variables */
 flush_MIDI();                                  /* Start clean */
 if ((p = malloc(maxmem)) == 0) exit(1);        /* Get memory or die    */

 head = p;                                      /* Save starting point  */
 for(x=0; x<7; x++)
  Bconout(3,reqst[x]);                          /* Request Dump All */

 while (x == x)                                 /* Spin wheels */
 {
  strm = Bconin(3) & 0x00FF;                    /* Ignore a masked MIDI byte */
  if (strm == 0xF0)                             /* If we get a SYS EX... */
  {
   *p = strm; ++p; ++byts; break;               /* Save it, count it, & exit */
  }
 }

 while (x == x)                                 /* Ad nauseum */
 {
  strm = Bconin(3) & 0x00FF;                    /* Get a masked MIDI byte */
  *p = strm;                                    /* Write it to array */
  ++p;                                          /* Increment the pointer */
  ++byts;                                       /* Count bytes received */
  if (strm == 0xFE) break;                      /* Break on active sensing */
 }

 printf("  So far, so good.  %u bytes in limbo\n",byts);
 printf("Korg dump (programs, combinations and sequences) now in memory.\n");
 printf("Enter name of destination file [d:m1all.dmp]: ");
 fflush(stdout);                                /* Ask it NOW! */
 strcpy(fid,"d:m1all.dmp");                     /* Use default name.    */
 if (fgets(buf,sizeof(buf),stdin) != NULL)
   sscanf(buf,"%s",fid);                        /* Or, use a real name  */
 p = head;                                      /* Rewind the psuedofile*/
 fout = fopen(fid,"wb");                        /* Where to dump to     */

 byts= 0;
 while((strm = *p) != 0xFE)                     /* REALLY save the file */
 {
  fputc(strm,fout); ++p;
  ++byts;  printf("%u\n",byts);                 /***DEBUG***/
 }
 fclose(fout);

 printf("Dump saved to %s. %u bytes\n",fid,byts);       /* Print totals */
 free(p);  exit();
/* appl_exit();                                 / Exit GEM nicely */
}
