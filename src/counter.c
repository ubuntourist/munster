/************************************************************************
 *                                                                      *
 *       counter.c              Written by Kevin Cole   04-Jan-92       *
 *                                                                      *
 *  This program is based on reader.c.  It's supposed to stuff a MIDI   *
 * dump into an Atari file...                                           *
 *                                                                      *
 ************************************************************************/

#include "osbind.h"
#include <stdio.h>

int gflag;                                      /* global flag */

from_MIDI()
{
 int ifm;  ifm = Bconin(3) & 0x00FF;  return(ifm);
}

MIDI_status()
{
 int ims;  ims = Bconin(3); if(ims != 0) ims = 1; return(ims);
}

check_MIDI()
{
 int flag,ret;
 flag = MIDI_status();
 switch(flag)
 {
  case 0:
    gflag = 1;
    break;
  default:
    ret = from_MIDI();
    break;
 }
 return(ret);
}

flush_MIDI()
{
 gflag = 0; while (gflag = 0) check_MIDI();
}

main()
{
FILE *fout;
unsigned int x,byts,eoxs,strm;

 x = 0; byts = 0; eoxs = 0;                     /* Initialize variables */
 flush_MIDI();                                  /* Start clean */
 fout = fopen("e:m1all.dmp","wb");              /* Where to dump to */

 while (x == x)                                 /* Spin wheels */
 {
  strm = Bconin(3) & 0x00FF;                    /* Get masked MIDI */
  if (strm == 0xF0) break;                      /* SYSEX stops spinning */
 }

 while (x == x)                                 /* Ad nauseum */
 {
  strm = Bconin(3) & 0x00FF;                    /* Get a MIDI byte */
  fputc(strm,fout);                             /* Write it to file */
/*  ++byts;                                     / Count bytes */
/*  if (strm == 0xF7) ++eoxs;                   / Count EOX's */
  if (strm == 0xFE) break;                      /* Break on active sensing */
 }

/* printf(" %u bytes, %d EOX's\n",byts,eoxs);   / Print totals */
 fclose(fout);
 exit();
}
