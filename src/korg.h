                /* MIDI Standard Code Specifications */

#define KEYOFF    0x80  // Note off         (1000 nnnn - 0kkk kkkk - 0100 0000)
#define KEYON     0x90  // Note on          (1001 nnnn - 0kkk kkkk - 0vvv vvvv)
#define POLYPRESS 0xA0
#define CTRLCHNG  0xB0  // Control change   (Many. Should expand this.)
#define PROGCHNG  0xC0  // Program change   (1100 nnnn - 0ppp pppp - ---- ----)
#define CHANPRESS 0xD0  // Channel pressure (1101 nnnn - 0vvv vvvv - ---- ----)
#define PITCHBND  0xE0  // Pitch bend       (1110 nnnn - 0bbb bbbb - 0bbb bbbb)
#define SYSMSG    0xF0  // System messages  (Many. Should expand this.)

#define SYSEX     0xF0
#define EOX       0xF7

                /* Korg M1 System Exclusive stuff */

/* Header (ID) information */

#define KORGID    0x42  // Korg   ID
#define FMTID     0x30  // Format ID (0011 nnnn)
#define M1ID      0x19  // M1     ID

/* Function Codes - Requests */

#define REQMODE   0x12  // Mode request
#define REQ1PGM   0x10  // Program parameter dump request - one program
#define REQPGMS   0x1C  // Program parameter dump request - all programs
#define REQ1CMB   0x19  // Combination parameter dump request - one combo
#define REQCMBS   0x1D  // Combination parameter dump request - all combos
#define REQSEQS   0x18  // Sequence data dump request - all sequences
#define REQGLBL   0x0E  // Global dump request
#define REQALL    0x0F  // All dump request (global, combos, programs, seqs)

/* Function codes - Dumps */

#define DMPMODE   0x42  // Mode data
#define DMP1PGM   0x40  // Program parameter data - one program
#define DMPPGMS   0x4C  // Program parameter data - all programs
#define DMP1CMB   0x49  // Combination parameter data - one combination
#define DMPCMBS   0x4D  // Combination parameter data - all combinations
#define DMPSEQS   0x48  // Sequence data - all sequences
#define DMPGLBL   0x51  // Global data
#define DMPALL    0x50  // All data (global, combos, programs, sequences)

/* Function codes - Save a patch */

#define WRT1PGM   0x11  // Program write request - one program
#define WRT1CMB   0x1A  // Combination write request - one combination

/* Function codes - Change? */

#define CHGMODE   0x4E  // Mode change
#define CHGPARM   0x41  // Parameter change

/* Function codes - Status messages */

#define FMTERR    0x26  // Received message format error
#define LOADOK    0x23  // Data load completed okay
#define LOADERR   0x24  // Data load error
#define WRTOK     0x21  // Write completed okay
#define WRTERR    0x22  // Write error

                /* Control Destinations */

#define VEL_CTRL  0x07

                /* Data Masks */

#define STATEMASK         0x80
#define CHANNELMASK       0x0F
#define STATE_SELECT_MASK 0xF0

                /* Fundamental Routines */

#define midi_in(c)    c = Bconin(3) & 0x00FF
#define midi_out(c)   Bconout(3,c & 0x00FF)
#define midi_print(c) printf("%02x ",c & 0x00FF)


struct ndxstr
{
  unsigned char instno,
                key,
                pan,
                tune,
                level,
                decay,
                null;
};

struct drumstr
{
  struct ndxstr drum_ndx[30];
};


struct glbstr
{
  unsigned char  mstr_tune,
                 key_trans,
                 dmpr_polrty,
                 as_ped_1,
                 as_ped_2,
                 scale_type,
                 pure_type_key,
                 user_scale[12];
  unsigned char  endglb[2];
  struct drumstr drum[4];
};


struct cmbstr
{
};


struct pgmstr
{
};


struct seqstr
{
};


struct all
{
  struct glbstr global;
  struct cmbstr combi;
  struct pgmstr progam;
  struct seqstr seqnc;
};
