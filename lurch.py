#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  lurch.py
#
#  Less cleverly named than "munster" but somewhat suggesive regarding
#  the stuttering nature of the progress forward... The goal is to
#  decode old Korg M1 files produced by munster.
#
#  Copyright 2021 Kevin Cole <ubuntourist@hacdc.org> 2021.04.06
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

import os
import sys
import os.path             # File and directory manipulations
import binascii

__appname__    = "Lurch"
__module__     = ""
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2021"
__agency__     = "HacDC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "1.0"
__maintainer__ = "Kevin Cole"
__email__      = "ubuntourist@hacdc.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


# Korg M1 machine language constants

EXCLUSIVE = 0xF0
KORG_ID   = 0x42
FORMAT_ID = 0x30  # "0011 nnnn" where nnnn = Global channel
M1_ID     = 0x19
SYS_EX    = bytearray([EXCLUSIVE, KORG_ID, FORMAT_ID, M1_ID])

PRG_DUMP  = SYS_EX.copy() + bytearray([0x4C])  # All program parameter
CMB_DUMP  = SYS_EX.copy() + bytearray([0x4D])  # All combination parameter
ALL_DUMP  = SYS_EX.copy() + bytearray([0x50])  # All data


def signed(val, bits=8):
    """Return sign extended number"""
    sign_bit = 1 << (bits - 1)                        # 128
    return (val & (sign_bit - 1)) - (val & sign_bit)  # (x & 127) - (x & 128)


def save_prg(dump):
    """Save program data"""
    pass


def save_cmb(dump):
    """Save combination data"""
    pass


def save_all(dump):
    """Save all data"""
    pedal_functions = ["Program (combination) up",    # 00
                       "Program (combination) down",  # 01
                       "Sequencer start/stop",        # 02
                       "Effect 1 on/off",             # 03
                       "Effect 2 on/off",             # 04
                       "Volume",                      # 05
                       "VDF cutoff",                  # 06
                       "Effect 1 control",            # 07
                       "Effect 2 control",            # 08
                       "Data entry"]                  # 09

    scale_types = ["Equal temper 1",  # 00
                   "Equal temper 2",  # 01
                   "Pure major",      # 02
                   "Pure minor",      # 03
                   "User program"]    # 04

    scale_keys = ["C",       # 00
                  "C#/Db",   # 01
                  "D",       # 02
                  "D#/Eb",   # 03
                  "E",       # 04
                  "F",       # 05
                  "F#/Gb",   # 06
                  "G",       # 07
                  "G#/Ab",   # 08
                  "A",       # 09
                  "A#/Bb",   # 0A
                  "B"]       # 0B

    memory_allocation = dump[5]
    seq_size = dump[6] + (dump[7] << 7)

    # Global parameter

    offset = 8
    master_tune        = signed(dump[offset + 0])
    key_transpose      = signed(dump[offset + 1])
    damper_polarity    = signed(dump[offset + 2])
    assignable_pedal_1 = signed(dump[offset + 3])  # See pedal_functions
    assignable_pedal_2 = signed(dump[offset + 4])  # See pedal_functions
    scale_type         = signed(dump[offset + 5])  # See scale_types
    pure_type_key      = signed(dump[offset + 6])  # See scale_keys
    user_scale         = []
    for note in range(7, 19):
        user_scale.append(signed(dump[offset + note]))

    # DEBUG / TEST
    print("-" * 78)
    for b in range(offset + 6):
        print(f"{b:02X}: {dump[b]:02X}")
    print("-" * 78)
    print(f"Sequence data size: {seq_size} bytes")
    print(f"Master tune:        {master_tune}")
    print(f"Key transposition:  {key_transpose}")
    print(f"Damper polarity:    {damper_polarity}")
    print(f"Assignable pedal 1: {pedal_functions[assignable_pedal_1]}")
    print(f"Assignable pedal 2: {pedal_functions[assignable_pedal_2]}")
    print(f"Scale type:         {scale_types[scale_type]}")
    print(f"Pure key type:      {scale_keys[pure_type_key]}")
#   for note in range(7, 19):
#       user_scale.append(signed(dump[offset + note]))
    print("-" * 78)


def main():
    _ = os.system("clear")
    print(f"{__appname__} v.{__version__}")
    print(f"{__copyright__} ({__license__})")
    print(f"{__author__}, {__agency__} <{__email__}>")
    print()

    if len(sys.argv) != 2:
        print("Usage:")
        print("    lurch (<file>.prg | <file>.cmb | <file>.all)")
        sys.exit(1)            # Two arguments

    print(f"Coverting {sys.argv[1]}")
    src = open(sys.argv[1], "rb")
    dump = src.read()
    src.close()

    mask = bytearray([0xFF, 0xFF, 0xF0, 0xFF, 0xFF])
    masked = bytes(bits & bitmask for (bits, bitmask) in zip(dump[:5], mask))
    if masked == PRG_DUMP:
        if sys.argv[1][-4:] != ".prg":
            print("WARNING: Program Dump should have file extention .prg")
        save_prg(dump)
    elif masked == CMB_DUMP:
        if sys.argv[1][-4:] != ".cmb":
            print("WARNING: Combination Dump should have file extention .cmb")
        save_cmb(dump)
    elif masked == ALL_DUMP:
        if sys.argv[1][-4:] != ".all":
            print("WARNING: Combination Dump should have file extention .all")
        save_all(dump)
    else:
        print("ERROR: Invalid file format")
        print(f"INP {binascii.hexlify(masked)} not one of:")
        print(f"PRG {binascii.hexlify(PRG_DUMP)}")
        print(f"CMB {binascii.hexlify(CMB_DUMP)}")
        print(f"ALL {binascii.hexlify(ALL_DUMP)}")
        sys.exit(1)

    # [Your code here]

    return 0


if __name__ == "__main__":
    main()
