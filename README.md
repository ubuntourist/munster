# Munster (M1-ST-er)

This is a weak attempt to preserve and perhaps resurrect a project
from 1992.

**munster** was a program to backup and restore the memory of eight
Korg M1 symthesizers from and to an Atari 1040-ST via MIDI. The
memory of the M1 contains both _instruments_ (including waveforms,
ASDR settings, and other parameters), and _sequences_ which are
essentially melodies / songs.

The dust has been settling on this code for 25 years, and much has
been forgotten.  A lot of it can be gotten rid of, but I'm not sure
how much any more.  The most important pieces here are:

	munster.c
	korg.h
	osbind.h
	vax.h

In addition, the `Data` directory contains the dumps from the 1992
Spring concert "Songs of the Rose" performed at American University,
Washington, DC.
