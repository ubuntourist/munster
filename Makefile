# Written by Kevin Cole <ubuntourist@hacdc.org> 2019.10.28 (kjc)
#

# Header files
#     korg.h
#     misc.h
#     osbind.h
#     vax.h

analyze : analyze.c osbind.h misc.h korg.h
	cc -o analyze

analyze2 : analyze2.c osbind.h misc.h korg.h
	cc -o analyze2

analyze3 : analyze3.c osbind.h misc.h korg.h
	cc -o analyze3

channels : channels.c osbind.h korg.h
	cc -o channels

counter : counter.c osbind.h
	cc -o counter

damnit : damnit.c osbind.h
	cc -o damnit

debug : debug.c osbind.h
	cc -o debug

debugry : debugry.c osbind.h korg.h
	cc -o debugry

decypher : decypher.c
	cc -o decypher

dump2 : dump2.c osbind.h korg.h
	cc -o dump2

dumper : dumper.c osbind.h misc.h korg.h
	cc -o dumper

dumper0 : dumper0.c osbind.h korg.h
	cc -o dumper0

dumper1 : dumper1.c osbind.h korg.h
	cc -o dumper1

dumper2 : dumper2.c osbind.h korg.h
	cc -o dumper2

dumperx : dumperx.c osbind.h korg.h
	cc -o dumperx

dumpery : dumpery.c osbind.h korg.h
	cc -o dumpery

expand : expand.c
	cc -o expand

explore : explore2.c
	cc -o explore2

explore2 : explore.c
	cc -o explore

explorex : explorex.c
	cc -o explorex

filer : filer.c osbind.h
	cc -o filer

fixme : fixme.c osbind.h
	cc -o fixme

howmuch : howmuch.c osbind.h
	cc -o howmuch

loader : loader0.c osbind.h
	cc -o loader0

loader0 : loader1.c osbind.h korg.h
	cc -o loader1

loader1 : loader.c osbind.h misc.h korg.h
	cc -o loader

loaderx : loaderx.c osbind.h korg.h
	cc -o loaderx

menuex : menuex.c
	cc -o menuex

munster : munster.c osbind.h korg.h
	cc -o munster

reader : reader.c osbind.h
	cc -o reader

readkey : readkey.c osbind.h
	cc -o readkey

scaler : scaler.c osbind.h
	cc -o scaler

search : search.c
	cc -o search

selfdoc : selfdoc.c
	cc -o selfdoc

stripper : stripper.c osbind.h korg.h
	cc -o stripper

term : term.c
	cc -o term

uudec : uudec.c
	cc -o uudec

uudecode : uudecode.c
	cc -o uudecode

writer : writer.c osbind.h
	cc -o writer

clean:
	rm analyze2 analyze3 analyze channels counter damnit debug \
	   debugry decypher dump2 dumper0 dumper1 dumper2 dumper   \
	   dumperx dumpery expand explore2 explore explorex filer  \
	   fixme howmuch loader0 loader1 loader loaderx menuex     \
	   munster reader readkey scaler search selfdoc stripper   \
	   term uudec uudecode writer
